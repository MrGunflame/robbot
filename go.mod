module gitlab.com/MrGunflame/robbot

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/go-cmp v0.5.2
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jasonlvhit/gocron v0.0.1
	gitlab.com/MrGunflame/gw2api v0.2.5
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/sys v0.0.0-20200810151505-1b9f1253b3ed // indirect
)
