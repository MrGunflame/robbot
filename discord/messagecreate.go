package discord

import "github.com/bwmarrin/discordgo"

// MessageCreate wraps discordgo.MessageCreate
type MessageCreate discordgo.MessageCreate

// Message wraps discordgo.Message
type Message discordgo.Message
