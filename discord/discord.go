package discord

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Session is a discord session
type Session struct {
	s *discordgo.Session // S is the core discordgo session
}

// NewSession creates a new session
func NewSession(s *discordgo.Session) *Session {
	return &Session{
		s: s,
	}
}

// Bare returns the underlaying dgo session
func (s *Session) Bare() *discordgo.Session {
	return s.s
}

// InvalidCommandUsage returns the error for invalid command usage
func (s *Session) InvalidCommandUsage() error {
	return ErrInvalidCommandUsage
}

// ChannelMessageSend sends a message to a channel
func (s *Session) ChannelMessageSend(chID string, msg ...string) error {
	// Make a new string buffer
	var b strings.Builder
	for _, s := range msg {
		b.WriteString(s)
	}

	_, err := s.s.ChannelMessageSend(chID, b.String())
	return err
}

// Respond reponds to a user in the format of `<@uID>, msg`
// chID is the channel id to send the message
// uID is the user id to mention
// msg is a string array for the message to be send
func (s *Session) Respond(chID, uID string, msg ...string) error {
	// Make a new string buffer
	var b strings.Builder
	for _, s := range []string{"<@", uID, ">, "} {
		b.WriteString(s)
	}

	for _, s := range msg {
		b.WriteString(s)
	}

	_, err := s.s.ChannelMessageSend(chID, b.String())
	return err
}

// ReactionResponse sends a number of reactions and waits for the user to add one
// It then returns the number of the reaction added
func (s *Session) ReactionResponse(msgStr, chID, uID string) (int, error) {
	msg, err := s.s.ChannelMessageSend(chID, msgStr)
	if err != nil {
		return 0, err
	}

	if err := s.s.MessageReactionAdd(chID, msg.ID, "startReaction"); err != nil {
		return 0, err
	}

	return 0, nil
}

// Success sends a success message to a channel
func (s *Session) Success(chID string, msg ...string) {
	var b strings.Builder
	b.WriteString(":white_check_mark: ")
	for _, s := range msg {
		b.WriteString(s)
	}
	s.s.ChannelMessageSend(chID, b.String())
}

// Failure sends a fail message to a channel
func (s *Session) Failure(chID string, msg ...string) {
	var b strings.Builder
	b.WriteString(":x: ")
	for _, s := range msg {
		b.WriteString(s)
	}
	s.s.ChannelMessageSend(chID, b.String())
}
