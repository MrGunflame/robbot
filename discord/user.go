package discord

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func newUser(st *discordgo.User) *User {
	return &User{
		ID:            st.ID,
		Username:      st.Username,
		Avatar:        st.Avatar,
		Discriminator: st.Discriminator,
		Token:         st.Token,
		Verified:      st.Verified,
		Bot:           st.Bot,
		// PublicFlags:   st.PublicFlags,
		// PremiumType: st.PremiumType,
		// System: st.System,
		// Flags: st.Flags,
	}
}

// User contains data for a discord user
type User struct {
	ID            string
	Username      string
	Avatar        string
	Discriminator string
	Token         string
	Verified      bool
	Bot           bool

	// https://discord.com/developers/docs/resources/user#user-object-user-flags
	PublicFlags int

	PremiumType int
	System      bool
	Flags       int
}

// Mention returns a mention to the user
func (u *User) Mention() string {
	return util.ConcatStrings("<@", u.ID, ">")
}

// UserChannelCreate creates a new private channel with a user
func (s *Session) UserChannelCreate(userID string) (*discordgo.Channel, error) {
	fmt.Println(s)
	return s.s.UserChannelCreate(userID)
}
