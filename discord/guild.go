package discord

import "github.com/bwmarrin/discordgo"

// Guild returns a guild
func (s *Session) Guild(guildID string) (*discordgo.Guild, error) {
	return s.s.Guild(guildID)
}

func (s *Session) GuildMember(guildID, userID string) (*Member, error) {
	st, err := s.s.GuildMember(guildID, userID)
	return newMember(st), err
}

// func (s *Session) GuildMemberDeafen(guildID, userID string, deaf bool) error {
// 	// return s.s.GuildMemberDeafen()
// 	return nil
// }

// GuildMemberMove moves a guild member into another channel
// if channelID is nil, he is removed from all channels instead
func (s *Session) GuildMemberMove(guildID, userID string, channelID *string) error {
	return s.s.GuildMemberMove(guildID, userID, channelID)
}

// GuildMemberMute changes the mute status of a user in a guild
// func (s *Session) GuildMemberMute(guildID, userID string, mute bool) error {
// 	// return s.s.GuildMemberMute(guildID, userID, mute)
// 	return nil
// }

func (s *Session) GuildMemberNickname(guildID, userID, nickname string) error {
	return s.s.GuildMemberNickname(guildID, userID, nickname)
}

func (s *Session) GuildMemberRoleAdd(guildID, userID, roleID string) error {
	return s.s.GuildMemberRoleAdd(guildID, userID, roleID)
}

func (s *Session) GuildMemberRoleRemove(guildID, userID, roleID string) error {
	return s.s.GuildMemberRoleRemove(guildID, userID, roleID)
}

// GuildMemberKick kicks a user from a guild
func (s *Session) GuildMemberKick(guildID, userID string) error {
	return s.s.GuildMemberDelete(guildID, userID)
}
