package discord

import "time"

// FormatTimeDefault formats the time into a readable version of ISO8601
func FormatTimeDefault(t time.Time) string {
	return t.Format("2006-01-02 15:04:05 MST")
}
