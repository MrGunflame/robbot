package discord

import (
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/MrGunflame/robbot/pkg/util"
)

var (
	regexArgsSplitter   = regexp.MustCompile(`(\"[^\"]*\"|[^\s]+)`)
	regexUserMention    = regexp.MustCompile(`<@!?\d+>`)
	regexRoleMention    = regexp.MustCompile(`<@&\d+>`)
	regexChannelMention = regexp.MustCompile(`<#\d+>`)
)

// type ArgumentType uint8

// const (
// 	ArgumentTypeString ArgumentType = iota
// 	ArgumentTypeInt
// 	ArgumentUserID
// 	ArgumentRoleID
// 	ArgumentChannelID
// )

// A Argument is a argument for a command parsed from a discord message
type Argument struct {
	raw string
}

// String returns the raw input argument
func (a *Argument) String() string {
	return a.raw
}

// Int returns the argument as an int
func (a *Argument) Int() (int, error) {
	return strconv.Atoi(a.raw)
}

// UserID returns the argument as an userid
func (a *Argument) UserID() (string, bool) {
	return util.ExtractUserID(a.raw), regexUserMention.MatchString(a.raw)
}

// RoleID returns the argument as a roleid
func (a *Argument) RoleID() (string, bool) {
	return util.ExtractRoleID(a.raw), regexRoleMention.MatchString(a.raw)
}

// ChannelID returns the argument as a channelid
func (a *Argument) ChannelID() (string, bool) {
	return util.ExtractChannelID(a.raw), regexChannelMention.MatchString(a.raw)
}

// Arguments contains multiple arguments
type Arguments struct {
	args []*Argument
}

// Len returns the number of all arguments
func (a *Arguments) Len() int {
	return len(a.args)
}

// Get returns a single argument
func (a *Arguments) Get(i int) *Argument {
	if i >= a.Len() {
		return nil
	}
	return a.args[i]
}

// PopFirst splits off the first argument from the argument list and returns it
func (a *Arguments) PopFirst() *Argument {
	arg := a.Get(0)
	if arg == nil {
		return &Argument{}
	}

	a.args = a.args[1:]
	return arg
}

// Next returns true while arguments still contains arguments
func (a *Arguments) Next() bool {
	return a.Len() > 0
}

// ParseArguments splits a string into multiple arguments
func ParseArguments(raw string) *Arguments {
	sl := regexArgsSplitter.FindAllString(raw, -1)

	var args []*Argument
	for _, s := range sl {
		// Replace the "" at the start/end of argument
		if strings.HasPrefix(s, "\"") && strings.HasSuffix(s, "\"") {
			s = s[1 : len(s)-1]
		}

		args = append(args, &Argument{
			raw: s,
		})
	}

	return &Arguments{
		args: args,
	}
}
