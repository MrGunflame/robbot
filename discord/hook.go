package discord

// EventType is a discord api event
type EventType uint8

// All avaliable events that can be used to trigger hooks
// See https://discord.com/developers/docs/topics/gateway#event-names for more information about each event
const (
	EventTypeChannelCreate EventType = iota
	EventTypeChannelUpdate
	EventTypeChannelDelete
	EventTypeChannelPinsUpdate
	EventTypeGuildCreate
	EventTypeGuildUpdate
	EventTypeGuildDelete
	EventTypeGuildBanAdd
	EventTypeGuildBanRemove
	EventTypeGuildEmojisUpdate
	EventTypeGuildIntegrationsUpdate
	EventTypeGuildMemberAdd
	EventTypeGuildMemberRemove
	EventTypeGuildMemberUpdate
	EventTypeGuildRoleCreate
	EventTypeGuildRoleDelete
	EventTypeInviteCreate
	EventTypeInviteDelete
	EventTypeMessageCreate
	EventTypeMessageUpdate
	EventTypeMessageDelete
	EventTypeMessageReactionAdd
	EventTypeMessageReactionRemove
	EventTypeMessageReactionRemoveAll
	EventTypeMessageReactionRemoveEmoji
	EventTypePresenceUpdate
	EventTypeTypingStart
	EventTypeUserUpdate
	EventTypeVoiceStateUpdate
	EventTypeVoiceServerUpdate
	EventTypeWebEventsUpdate
)

// A Hook is an event that is triggered by a discord event
type Hook struct {
	TriggerEvent uint8
}
