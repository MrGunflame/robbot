package discord

import (
	"github.com/bwmarrin/discordgo"
)

// NewEmbed creates a new embed
func NewEmbed(title, description string) *Embed {
	return &Embed{
		Title: title,
	}
}

// Embed is a discordgo embed
type Embed discordgo.MessageEmbed

// type Embed struct {
// 	URL         string
// 	Type        EmbedType
// 	Title       string
// 	Description string
// 	Timestamp   string
// 	Color       int
// 	Footer      *EmbedFooter
// 	Image       *EmbedImage
// 	Thumnail    *EmbedThumbnail
// 	Video       *EmbedVideo
// 	Provider    *EmbedProvider
// 	Author      *EmbedAuthor
// 	Fields      []*EmbedField
// }

type EmbedType interface{}

type EmbedFooter struct {
	Text         string
	IconURL      string
	ProxyIconURL string
}

type EmbedImage struct {
	URL      string
	ProxyURL string
	Width    int
	Height   int
}

type EmbedThumbnail struct {
	URL      string
	ProxyURL string
	Width    int
	Height   int
}

type EmbedVideo struct {
	URL    string
	Width  int
	Height int
}

type EmbedProvider struct {
	URL  string
	Name string
}

type EmbedAuthor struct {
	URL          string
	Name         string
	IconURL      string
	ProxyIconURL string
}

type EmbedField struct {
	Name   string
	Value  string
	Inline bool
}

// ChannelMessageSendEmbed sends a embed in a channel
func (s *Session) ChannelMessageSendEmbed(chID string, embed *Embed) (*discordgo.Message, error) {
	dgo := discordgo.MessageEmbed(*embed)
	msg, err := s.s.ChannelMessageSendEmbed(chID, &dgo)
	return msg, err
}

// DefaultEmbedFooter returns a new footer with the bots icon and optional text
func DefaultEmbedFooter(text string) *discordgo.MessageEmbedFooter {
	return &discordgo.MessageEmbedFooter{
		Text:         text,
		IconURL:      "https://cdn.discordapp.com/avatars/658364234010460170/ba50294f0dbfdab4fe3c51043964e8dd.webp?size=64",
		ProxyIconURL: "https://cdn.discordapp.com/avatars/658364234010460170/ba50294f0dbfdab4fe3c51043964e8dd.webp?size=64",
	}
}
