package discord

import "gitlab.com/MrGunflame/robbot/store"

// A Module is a namespace for creating custom commands and bot functionalities
type Module struct {
	Name        string              // The name of the module
	Description string              // Module Description
	GuildOnly   bool                // Whether the module may only be used in a guild
	Commands    map[string]*Command // All commands of the module
	Permissions []string
	Store       *store.Store
}

// A Command is a command of a module that responds to a user calling it
type Command struct {
	Name        string
	Description string
	Usage       string
	Example     string
	Permissions []string
	Execute     func(*Session, *MessageCreate, *Arguments) error
}
