package discord

import (
	"github.com/bwmarrin/discordgo"
)

func newMember(st *discordgo.Member) *Member {
	return &Member{
		GuildID:      st.GuildID,
		JoinedAt:     string(st.JoinedAt),
		Nick:         st.Nick,
		Deaf:         st.Deaf,
		Mute:         st.Mute,
		User:         newUser(st.User),
		Roles:        st.Roles,
		PremiumSince: string(st.PremiumSince),
	}
}

type Member struct {
	GuildID      string
	JoinedAt     string
	Nick         string
	Deaf         bool
	Mute         bool
	User         *User
	Roles        []string
	PremiumSince string
}

func (m *Member) Mention() string {
	return m.User.Mention()
}
