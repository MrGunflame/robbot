package discord

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestArgumentString(t *testing.T) {
	cases := []struct {
		given  *Argument
		expect string
	}{
		{
			given: &Argument{
				raw: "Test",
			},
			expect: "Test",
		},
	}

	for _, c := range cases {
		result := c.given.String()
		if result != c.expect {
			t.Errorf("String failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestArgumentInt(t *testing.T) {
	cases := []struct {
		given  *Argument
		expect struct {
			i   int
			err error
		}
	}{
		{
			given: &Argument{
				raw: "1",
			},
			expect: struct {
				i   int
				err error
			}{
				i:   1,
				err: nil,
			},
		},
	}

	for _, c := range cases {
		i, err := c.given.Int()
		if i != c.expect.i || err != c.expect.err {
			t.Errorf("Int failed: '%d', '%s' was expected but '%d', '%s' was returned", c.expect.i, c.expect.err, i, err)
		}
	}
}

func TestArgumentUserID(t *testing.T) {
	cases := []struct {
		given  *Argument
		expect string
	}{
		{
			given: &Argument{
				raw: "<@1234>",
			},
			expect: "1234",
		},
	}

	for _, c := range cases {
		result, _ := c.given.UserID()
		if result != c.expect {
			t.Errorf("UserID failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestArgumentRoleID(t *testing.T) {
	cases := []struct {
		given  *Argument
		expect string
	}{
		{
			given: &Argument{
				raw: "<@&1234>",
			},
			expect: "1234",
		},
	}

	for _, c := range cases {
		result, _ := c.given.RoleID()
		if result != c.expect {
			t.Errorf("RoleID failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestArgumentChannelID(t *testing.T) {
	cases := []struct {
		given  *Argument
		expect string
	}{
		{
			given: &Argument{
				raw: "<#1234>",
			},
			expect: "1234",
		},
	}

	for _, c := range cases {
		result, _ := c.given.ChannelID()
		if result != c.expect {
			t.Errorf("ChannelID failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestArgumentsLen(t *testing.T) {
	cases := []struct {
		given  *Arguments
		expect int
	}{
		{
			given: &Arguments{},
		},
	}

	for _, c := range cases {
		result := c.given.Len()
		if result != c.expect {
			t.Errorf("Len failed: '%d' was expected but '%d' was returned", c.expect, result)
		}
	}
}

func TestArgumentsGet(t *testing.T) {
	cases := []struct {
		given  *Arguments
		expect *Argument
	}{
		{
			given:  &Arguments{},
			expect: nil,
		},
		{
			given: &Arguments{
				args: []*Argument{
					{
						raw: "Test1",
					},
				},
			},
			expect: &Argument{
				raw: "Test1",
			},
		},
	}

	for _, c := range cases {
		result := c.given.Get(0)
		if !cmp.Equal(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})) {
			t.Errorf("Get failed: '%s'", cmp.Diff(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})))
		}
	}
}

func TestArgumentsPopFirst(t *testing.T) {
	cases := []struct {
		given  *Arguments
		expect *Argument
	}{
		{
			given: &Arguments{
				args: []*Argument{
					{
						raw: "Test1",
					},
					{
						raw: "Test2",
					},
				},
			},
			expect: &Argument{
				raw: "Test1",
			},
		},
	}

	for _, c := range cases {
		result := c.given.PopFirst()
		if !cmp.Equal(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})) {
			t.Errorf("PopFirst failed: '%s'", cmp.Diff(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})))
		}
	}
}

func TestParseArguments(t *testing.T) {
	cases := []struct {
		given  string
		expect *Arguments
	}{
		{
			given: "Hello World",
			expect: &Arguments{
				args: []*Argument{
					{
						raw: "Hello",
					},
					{
						raw: "World",
					},
				},
			},
		},
		{
			given: "\"Hello World\"",
			expect: &Arguments{
				args: []*Argument{
					{
						raw: "Hello World",
					},
				},
			},
		},
	}

	for _, c := range cases {
		result := ParseArguments(c.given)
		if !cmp.Equal(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})) {
			t.Errorf("ParseArguments failed: '%s'", cmp.Diff(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})))
		}
	}
}
