package discord

// ChannelTyping starts typing in a channel
func (s *Session) ChannelTyping(chID string) error {
	return s.s.ChannelTyping(chID)
}

// ChannelMessageDelete deletes a message in a channel
func (s *Session) ChannelMessageDelete(chID, msgID string) error {
	return s.s.ChannelMessageDelete(chID, msgID)
}
