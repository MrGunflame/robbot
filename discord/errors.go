package discord

import "errors"

// Static response errors
var (
	ErrInvalidCommandUsage = errors.New("Invalid command message")
)
