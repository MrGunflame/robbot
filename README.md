# Discord Robbot

## Overwiew
Robbot is a fast and modular static-built Discord Bot built in Go. All features are provided through modules, custom sets of commands and routines. You can even bring your own modules, making Robbot fully customizable to your needs and preferences.

## Installation

### Requirements
- go (Build only)
- MySQL 5.7+ or MariaDB 10.2.7+

### Preparation
Since all modules are compiled into the main binary you need to decide which modules to include before you run the build. A config tool is included to easily add and remove modules before building.

- `git clone https://gitlab.com/MrGunflame/robbot`
- `make configure`

Now you can add or remove modules to your liking using `./configure`.

### Building

#### Linux
- `make build`

#### Docker
- `make docker`

### Running it

Go to [the Discord Developer Portal](https://discord.com/developers/applications) and create a new application. Make it a bot and copy the bot token (not the application secret) and paste it into `config/bot.json`.

## Adding custom modules

Expanding the bot should be done via modules. Example modules can be found [in the examples directory](https://gitlab.com/MrGunflame/robbot/-/tree/master/examples).

Documentation for all required packages can be found on GoDoc:
Module Configuration: https://godoc.org/gitlab.com/MrGunflame/robbot/bot
Discord Bindings: https://godoc.org/gitlab.com/MrGunflame/robbot/discord
