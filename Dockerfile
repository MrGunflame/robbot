FROM golang:1.15 as builder
LABEL stage=builder
RUN mkdir -p /src
WORKDIR /src
COPY . .
RUN make build

FROM alpine:latest
WORKDIR /
COPY --from=builder /src/robbot .
COPY config/ /config
CMD ["/robbot", "env"]
