package modules

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/modules/events"
	"gitlab.com/MrGunflame/robbot/modules/moderation"
	"gitlab.com/MrGunflame/robbot/modules/permissions"
)

// modules contains all the modules loaded
var modules = map[string]*bot.Module{
	"permissions": permissions.Module,
	"events":      events.Module,
	"moderation":  moderation.Module,
}
