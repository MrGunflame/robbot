package modules

import (
	"fmt"
	"log"

	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"

	"github.com/bwmarrin/discordgo"
	"github.com/jasonlvhit/gocron"
)

// This file contains the code that manages automatic task executing of each module

// SpawnTasks spawns all tasks provided by modules
func SpawnTasks(s *discord.Session) {
	gocron.Start()
	for _, m := range modules {
		for _, t := range m.Tasks {
			if t.Interval > 0 {
				gocron.Every(uint64(t.Interval.Seconds())).Seconds().From(gocron.NextTick()).Do(RunTask, s, util.ConcatStrings(m.Name, ".", t.Name), t.Execute, 0)
			}
		}
	}
}

// RunTask runs a task and checks for errors
func RunTask(s *discord.Session, name string, taskFn func(*discord.Session, string) error, call int) {
	guilds, err := s.Bare().UserGuilds(100, "", "")
	if err != nil {
		log.Println("[ERROR/TASK] Failed to load bot guilds:", err)
		return
	}

	for _, g := range guilds {
		go func(g *discordgo.UserGuild) {
			if err := taskFn(s, g.ID); err != nil {
				log.Println(fmt.Sprintf("[ERROR/TASK] Task %s returned an error while executing: '%s'", name, err))
				return
			}
			log.Println(fmt.Sprintf("[INFO/TASK] Task %s complete for guild %s (%s)", name, g.Name, g.ID))
		}(g)
	}
}
