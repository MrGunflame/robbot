package modules

import (
	"strings"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

// The builtin command [!]help
func help(s *discord.Session, m *discord.MessageCreate, args *bot.Arguments) error {
	// Module help
	if args.Next() {
		if mod, ok := modules[args.PopFirst().String()]; ok {
			// Command help
			if args.Next() {
				name := args.PopFirst().String()
				if cmd, ok := mod.Commands[name]; ok {
					s.ChannelMessageSendEmbed(m.ChannelID, commandHelp(name, cmd))
					return nil
				}
			}

			// Module help
			s.ChannelMessageSendEmbed(m.ChannelID, moduleHelp(mod))
			return nil
		}
	}

	// Global help
	var b strings.Builder
	b.WriteString("**__Commands:__**\n")
	for k := range coreCommands {
		for _, s := range []string{k, "\n"} {
			b.WriteString(s)
		}
	}

	b.WriteString("\n**__Enabled Modules:__**\n")
	for _, mod := range modules {
		for _, s := range []string{"**", mod.Name, ":** ", mod.Description, "\n"} {
			b.WriteString(s)
		}
	}

	s.ChannelMessageSendEmbed(m.ChannelID, &discord.Embed{
		Title:       "**Global Help**",
		Color:       util.ColorRed,
		Description: b.String(),
	})
	return nil
}

// moduleHelp generates a generic help message for a module
func moduleHelp(mod *bot.Module) *discord.Embed {
	var cmds []string
	for k := range mod.Commands {
		cmds = append(cmds, k)
	}

	return &discord.Embed{
		Title:       util.ConcatStrings("**__Module:__** ", mod.Name),
		Color:       util.ColorRed,
		Description: strings.Join([]string{util.ConcatStrings("**", mod.Description, "**"), "**Commands:**", strings.Join(cmds, ", ")}, "\n"),
	}
}

// commandHelp generates a generic help message for a command
func commandHelp(name string, cmd *bot.Command) *discord.Embed {
	return &discord.Embed{
		Title:       util.ConcatStrings("**__Command:__** ", name),
		Color:       util.ColorRed,
		Description: strings.Join([]string{util.ConcatStrings("**Description:** ", cmd.Description), util.ConcatStrings("**Usage:** ", cmd.Usage), util.ConcatStrings("**Example:** ", cmd.Example)}, "\n"),
	}
}
