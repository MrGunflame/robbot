package modules

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"

	"github.com/bwmarrin/discordgo"
)

var (
	buildVersion string
	buildDate    string
)

func version(s *discord.Session, m *discord.MessageCreate, _ *bot.Arguments) error {
	s.Bare().ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
		Title:       "Version",
		Color:       util.ColorRed,
		Description: util.ConcatStrings(buildVersion, "\nBuilt: ", buildDate),
	})

	return nil
}
