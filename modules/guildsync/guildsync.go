package guildsync

import (
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/mysql"
	"gitlab.com/MrGunflame/robbot/store"
)

const (
	permissionManage = "manage"
	permissionSync   = "sync"
)

// Module Guildsync allows to synchronise GW2 guild ranks with discord roles
var Module = &bot.Module{
	Name:        "guildsync",
	Description: "Manage Guild Wars 2 to Discord Account Links",
	GuildOnly:   true,
	Commands: map[string]*bot.Command{
		"verify": {
			Name:        "verify",
			Description: "Verify and link a Discord member with a guild member.",
			Usage:       "[User] [Accountname]",
			Example:     "@Slacker Slacker.1234",
			Execute:     verify,
			Permissions: []string{permissionManage},
		},
		"unverify": {
			Name:        "unverify",
			Description: "Unverify and unlink a Disord member.",
			Usage:       "[User]",
			Example:     "@Slacker",
			Execute:     unverify,
			Permissions: []string{permissionManage},
		},
		"whois": {
			Name:        "whois",
			Description: "Identify the account associated with Discord member.",
			Usage:       "[User]",
			Example:     "@Slacker",
			Execute:     whois,
			Permissions: []string{permissionManage},
		},
		"sync": {
			Name:        "sync",
			Description: "Synchronise everything. Now.",
			Usage:       "",
			Example:     "",
			Execute:     sync,
			Permissions: []string{permissionManage, permissionSync},
		},
		"list": {
			Name:        "list",
			Description: "List all registered users",
			Usage:       "",
			Example:     "",
			Execute:     list,
			Permissions: []string{permissionManage},
		},
	},
	Permissions: []string{permissionManage, permissionSync},
	Tasks: []*bot.Task{
		{
			Name:     "sync",
			Interval: 1 * time.Hour,
			Execute:  syncTask,
		},
	},
	Store: &store.Store{
		Tables: []*store.Table{
			// DEPRECEATED
			// ---
			{
				Name: "guildmembers",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "INT",
						Flags: []store.SQLFlag{store.SQLFlagAutoIncrement, store.SQLFlagNotNull, store.SQLFlagPrimaryKey, store.SQLFlagUnsigned},
					},
					{
						Name:  "guildwars2",
						Type:  "VARCHAR",
						Len:   36,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "discord",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
			// ---
			{
				Name: "guildsync_guilds",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "VARCHAR",
						Len:   36,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "disord_guild",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
			{
				Name: "guildsync_ranks",
				Columns: []*store.Column{
					{
						Name:  "name",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "id",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
			{
				Name: "guildsync_members",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "INT",
						Flags: []store.SQLFlag{store.SQLFlagAutoIncrement, store.SQLFlagNotNull, store.SQLFlagPrimaryKey, store.SQLFlagUnsigned},
					},
					{
						Name:  "name", // Account Name
						Type:  "VARCHAR",
						Len:   36,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "id", // Discord User ID
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
		},
	},
}

// GuildMember contains data of a user for syncing between discord and guildwars2
type GuildMember struct {
	ID         int
	GuildWars2 string
	Discord    string
}

// NewGuildMember creates a new guild member
func NewGuildMember(guildwars2, discord string) *GuildMember {
	return &GuildMember{
		GuildWars2: guildwars2,
		Discord:    discord,
	}
}

// Insert inserts a new guildmember into the database
func (d *GuildMember) Insert() (int, error) {
	return 0, mysql.AddGuildMember(d.GuildWars2, d.Discord)
}

// DeleteGuildMemberByAccountName removes a guildmember from the database
func DeleteGuildMemberByAccountName(accountName string) (int, error) {
	return 0, mysql.RemoveGuildMember(1, accountName)
}
