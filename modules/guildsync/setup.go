package guildsync

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/modules/token"
)

func setup(ctx *bot.Context) error {
	tokens, err := token.UserTokens(ctx.Event.Author.ID)
	if err != nil {
		return err
	}

	if len(tokens) < 1 {
		ctx.Failure("You have no tokens registered with your account. Use the token module to add one.")
		return nil
	} else if len(tokens) > 1 {
		ctx.Failure("You have multiple tokens registered with your account. Specify the token id in the setup command.")
		return nil
	}

	// 	tokens, err := token.GetTokensByUser(m.Author.ID)
	// 	if err != nil {
	// 		return err
	// 	}

	// 	if len(tokens) < 1 {
	// 		s.ChannelMessageSend(m.ChannelID, ":x: You have no tokens registered with your account. Use the token command to add one.")
	// 		return nil
	// 	}

	// 	if len(tokens) > 1 && len(args) < 1 {
	// 		_, err := s.ChannelMessageSend(m.ChannelID, "You have multiple tokens registered with your account. Write the number of your token after the command.")
	// 		if err != nil {
	// 			return err
	// 		}

	// 		// s.MessageReactionAdd(m.ChannelID, msg.ID, "1️⃣")
	// 	}

	// 	id, err := strconv.Atoi(args[0])
	// 	if err != nil {
	// 		s.ChannelMessageSend(m.ChannelID, util.ConcatStrings(":x: `", args[0], "` is no number."))
	// 		return nil
	// 	}

	// 	var token string
	// 	for _, t := range tokens {
	// 		if t.ID == id {
	// 			token = t.Token
	// 			break
	// 		}
	// 	}

	// 	// acc, err := gw2api.New().WithAccessToken(token).Account()
	// 	// if err != nil {
	// 	// 	return err
	// 	// }

	// 	// for _, gID := range acc.GuildLeader {
	// 	// 	guild, err := gw2api.New().WithAccessToken(token).GetGuild()
	// 	// }

	return nil
}
