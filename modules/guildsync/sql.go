package guildsync

import "gitlab.com/MrGunflame/robbot/store"

type guild struct {
	id    string // GW2 UID
	guild string // Discord Guild
	owner string // Discord User ID of the Owner to determine API token
}

func newGuild(id, guildID, userID string) *guild {
	return &guild{
		id:    id,
		guild: guildID,
		owner: userID,
	}
}

type guildMember struct {
	ID         int
	GuildWars2 string
	Discord    string
}

func newGuildMember(id, account string) *guildMember {
	return &guildMember{
		Discord:    id,
		GuildWars2: account,
	}
}

func (d *guildMember) insert() (int, error) {
	return store.Insert("INSERT INTO guildsync_members (guildwars2, discord) VALUES (?, ?)", d.GuildWars2, d.Discord)
}

func getGuildMembers(guild string) ([]*guildMember, error) {
	rows, err := store.Select("SELECT id, guildwars2, discord FROM guildsync_members")
	if err != nil {
		return nil, err
	}

	var members []*guildMember
	for rows.Next() {
		var m guildMember
		if err := rows.Scan(&m.ID, &m.GuildWars2, &m.Discord); err != nil {
			return nil, err
		}
		members = append(members, &m)
	}

	return members, nil
}

// Remove member with given discord id
func removeGuildMember(id, guild string) (int, error) {
	return store.Delete("DELETE FROM guildsync_members WHERE discord = ?", id)
}
