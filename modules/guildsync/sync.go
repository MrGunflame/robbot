package guildsync

import (
	"gitlab.com/MrGunflame/robbot/bot"
)

func sync(ctx *bot.Context) error {
	ctx.RunTask(syncTask)
	return nil
}
