package guildsync

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/mysql"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func list(ctx *bot.Context) error {
	ctx.Typing()

	// Get the users from the database
	users, err := mysql.GetGuildMembers()
	if err != nil {
		return err
	}

	// Send a big embed with all entries
	util.NewBigEmbed("__Linked Accounts__", util.ColorRed, len(users), func(i int) string {
		return util.ConcatStrings("**", users[i].GuildWars2, "**: <@", users[i].Discord, ">\n")
	}, (2+32+6+18+3)).Send(ctx.Session.Bare(), ctx.Event.ChannelID)

	return nil
}
