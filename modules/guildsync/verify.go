package guildsync

import (
	"fmt"

	"gitlab.com/MrGunflame/gw2api"
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/config"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func verify(ctx *bot.Context) error {
	id, ok := ctx.Arguments.PopFirst().UserID()
	name := ctx.Arguments.String()
	if !ok || name == "" {
		return ctx.InvalidCommandUsage()
	}

	// Try to get guild members from cache
	gw2guild := config.Server[ctx.Event.GuildID].Guild.ID
	var guildMembers []*gw2api.GuildMember
	if val, ok := guildMemberCache.Get(gw2guild); ok {
		guildMembers = val.([]*gw2api.GuildMember)
	} else {
		members, err := gw2api.New().WithAccessToken(config.Server[ctx.Event.GuildID].Guild.ApiKey).GetGuildMembers(gw2guild)
		if err != nil {
			return err
		}
		guildMembers = members
		guildMemberCache.Insert(gw2guild, guildMembers)
	}

	ranks := config.Server[ctx.Event.GuildID].Guild.Role

	// Find the matching one
	for _, gM := range guildMembers {
		if gM.Name == name {
			// Add to database
			if _, err := NewGuildMember(name, id).Insert(); err != nil {
				return err
			}

			// Add Discord role
			for _, rk := range ranks {
				if err := ctx.Session.GuildMemberRoleAdd(ctx.Event.GuildID, id, rk.ID); err != nil {
					return err
				}
			}

			ctx.Success(fmt.Sprintf("Successfully linked %s with `%s`.", util.MentionUser(id), name))
			return nil
		}
	}

	ctx.Failure(fmt.Sprintf("Unable to find `%s` in Guild Rooster.", name))
	return nil
}
