package guildsync

import (
	"time"

	"gitlab.com/MrGunflame/robbot/pkg/util"
)

var guildMemberCache = util.NewCache(300 * time.Second)
