package guildsync

import (
	"gitlab.com/MrGunflame/gw2api"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/config"
	"gitlab.com/MrGunflame/robbot/pkg/mysql"
)

func syncTask(s *discord.Session, guild string) error {
	// Skip if not registered in config
	if _, ok := config.Server[guild]; !ok {
		return nil
	}

	// TODO: Move the following out of the file into DB tables
	guildID := config.Server[guild].Guild.ID
	guildToken := config.Server[guild].Guild.ApiKey
	ranks := config.Server[guild].Guild.Role

	// Get members from database
	members, err := mysql.GetGuildMembers()
	if err != nil {
		return err
	}

	// Get members from API
	guildMembers, err := gw2api.New().WithAccessToken(guildToken).GetGuildMembers(guildID)
	if err != nil {
		return err
	}

	// Get discord members
	discordMembers, err := s.Bare().GuildMembers(guild, "", 1000)
	if err != nil {
		return err
	}

	// TODO: Binary sort and search might help here (on members & guildMembers)
loop:
	for _, dM := range discordMembers {
		// Search in database members
	loop2:
		for _, m := range members {
			// Found
			if dM.User.ID == m.Discord {
				// Search in guild members
				for _, gM := range guildMembers {
					// Found
					if m.GuildWars2 == gM.Name {
						// roleID is the discord id of the role assigned by the guild rank
						var roleID string
						for _, rk := range ranks {
							if rk.Name == gM.Rank {
								roleID = rk.ID
								break
							}
						}

						// Remove the role when it is in ranks and NOT the assigned role (roleID)
						var found bool
						for _, r := range dM.Roles {
							for _, rk := range ranks {
								if r == rk.ID && rk.ID != roleID {
									if err := s.GuildMemberRoleRemove(guild, dM.User.ID, rk.ID); err != nil {
										return err
									}
								}

								// User already has correct role
								if r == roleID {
									found = true
								}
							}
						}

						// Add the assigned role
						if !found && roleID != "" {
							if err := s.GuildMemberRoleAdd(guild, dM.User.ID, roleID); err != nil {
								return err
							}
						}

						continue loop
					}
				}

				// Not found
				// DEPRECIATED
				// TODO: Switch to guildsync_members with removeMember
				if err := mysql.RemoveGuildMember(2, m.Discord); err != nil {
					return err
				}
				// ----

				break loop2
			}
		}

		// Not Found
		// Remove any roles that are registered as guild-linked ranks
		for _, r := range dM.Roles {
			for _, rk := range ranks {
				if r == rk.ID {
					if err := s.GuildMemberRoleRemove(guild, dM.User.ID, rk.ID); err != nil {
						return err
					}
				}
			}
		}
	}

	return nil
}
