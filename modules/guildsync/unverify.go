package guildsync

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/mysql"
)

func unverify(ctx *bot.Context) error {
	id, ok := ctx.Arguments.PopFirst().UserID()
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	if err := mysql.RemoveGuildMember(2, id); err != nil {
		return err
	}

	ctx.Success("Successfully unverified member")
	return nil
}
