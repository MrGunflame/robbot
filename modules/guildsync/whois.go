package guildsync

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/mysql"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func whois(ctx *bot.Context) error {
	if !ctx.Arguments.Next() {
		return ctx.InvalidCommandUsage()
	}

	// Get the account from the database
	member, err := mysql.GetGuildMember(ctx.Event.Mentions[0].ID)
	if err != nil {
		return err
	}

	ctx.Respond(fmt.Sprintf("%s is linked with `%s`.", util.MentionUser(ctx.Event.Mentions[0].ID), member.GuildWars2))
	return nil
}
