package token

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/store"
)

// Module token manages apikeys for users
var Module = &bot.Module{
	Name:        "token",
	Description: "Manage Guild Wars 2 API tokens",
	GuildOnly:   false,
	Commands: map[string]*bot.Command{
		"add": {
			Name:        "add",
			Description: "Add a new API token to your account.",
			Usage:       "<API Token>",
			Example:     "564F181A-F0FC-114A-A55D-3C1DCD45F3767AF3848F-AB29-4EBF-9594-F91E6A75E015",
			Execute:     add,
		},
		"list": {
			Name:        "list",
			Description: "List all your API tokens (as DM).",
			Usage:       "",
			Example:     "",
			Execute:     list,
		},
		"remove": {
			Name:        "remove",
			Description: "Remove an API token from your account.",
			Usage:       "<Token-ID>",
			Example:     "1",
			Execute:     remove,
		},
	},
	Store: &store.Store{
		Tables: []*store.Table{
			{
				Name: "token",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "INT",
						Flags: []store.SQLFlag{store.SQLFlagAutoIncrement, store.SQLFlagNotNull, store.SQLFlagPrimaryKey, store.SQLFlagUnsigned},
					},
					{
						Name:  "owner",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "token",
						Type:  "VARCHAR",
						Len:   128,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
		},
	},
}

// Token contains a gw2 token and the discord owner
type Token struct {
	ID    int
	Owner string
	Token string
	Flags uint64 // Permissions flags
}

func newToken(token, owner string) *Token {
	return &Token{
		Owner: owner,
		Token: token,
	}
}

func (d *Token) insert() (int, error) {
	return store.Insert("INSERT INTO token(owner, token) VALUES (?, ?)", d.Owner, d.Token)
}

// Tokens are always unique so it is safe to remove a single key that way
func deleteToken(token string) (int, error) {
	return store.Delete("DELETE FROM token WHERE token = ?")
}

func getTokens(owner string) ([]*Token, error) {
	rows, err := store.Select("SELECT id, owner, token FROM token WHERE owner = ? ORDER BY id ASC", owner)
	if err != nil {
		return nil, err
	}

	var tokens []*Token
	for rows.Next() {
		var t Token
		if err = rows.Scan(&t.ID, &t.Owner, &t.Token); err != nil {
			return nil, err
		}
		tokens = append(tokens, &t)
	}

	return tokens, nil
}
