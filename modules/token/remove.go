package token

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
)

func remove(ctx *bot.Context) error {
	// Extract the number
	n, ok := ctx.Arguments.PopFirst().Int()
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	// Get all tokens of the user
	tokens, err := getTokens(ctx.Event.Author.ID)
	if err != nil {
		return err
	}

	// Check if the number is valid
	if len(tokens) < (n - 1) {
		// s.ChannelMessageSend(m.ChannelID, fmt.Sprintf(":x: Unable to find token number `%s`.", args[0]))
		ctx.Failure(fmt.Sprintf("Cannot find token number `%d`.", n))
		return nil
	}

	// Delete from database
	if _, err := deleteToken(tokens[n].Token); err != nil {
		return err
	}

	ctx.Success("Successfully deleted the API token.")
	return nil
}
