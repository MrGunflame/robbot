package token

import (
	"strconv"
	"strings"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func list(ctx *bot.Context) error {
	// Create a new DM channel
	ch, err := ctx.Session.Bare().UserChannelCreate(ctx.Event.Author.ID)
	if err != nil {
		return err
	}

	// Get all tokens from the database
	tokens, err := getTokens(ctx.Event.Author.ID)
	if err != nil {
		return err
	}

	// Create a new buffer to write the description string to
	var b strings.Builder

	// Write a message when no tokens are present
	if len(tokens) < 1 {
		b.WriteString("You have no API tokens added yet. Use the token add command to add one.")
	}

	// Write each token to the buffer
	for i, t := range tokens {
		for _, s := range []string{"[**", strconv.Itoa(i + 1), "**]: ", t.Token, "\n"} {
			b.WriteString(s)
		}
	}

	ctx.Session.ChannelMessageSendEmbed(ch.ID, &discord.Embed{
		Title:       "Your API Tokens",
		Color:       util.ColorOrange,
		Description: b.String(),
		Footer:      discord.DefaultEmbedFooter(""),
	})

	return nil
}
