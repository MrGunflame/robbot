package token

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/MrGunflame/gw2api"
	"gitlab.com/MrGunflame/gw2api/util"
	"gitlab.com/MrGunflame/robbot/bot"
)

func add(ctx *bot.Context) error {
	if !ctx.Arguments.Next() {
		return ctx.InvalidCommandUsage()
	}

	// Remove message when sent outside of DMs
	if ch, err := ctx.Session.Bare().Channel(ctx.Event.ChannelID); err != nil {
		return err
	} else if ch.Type != discordgo.ChannelTypeDM {
		if err := ctx.Session.Bare().ChannelMessageDelete(ctx.Event.ChannelID, ctx.Event.ID); err != nil {
			ctx.Failure("Failed to delete your message.")
			return err
		}
	}

	// Extract the token
	token := ctx.Arguments.PopFirst().String()

	// Check if the token looks valid before making a request
	if !util.ValidAPIKey(token) {
		ctx.Failure("That doesn't look like a valid token.")
		return nil
	}

	// Validate the token
	_, err := gw2api.New().WithAccessToken(token).Tokeninfo()
	if err == gw2api.ErrInvalidAccessToken {
		ctx.Failure("Invalid api token")
		return nil
	} else if err != nil {
		return err
	}

	// Insert into database
	if _, err := newToken(token, ctx.Event.Author.ID).insert(); err != nil {
		return err
	}

	ctx.Success("Successfully added your new API token.")
	return nil
}
