package token

// UserTokens returns all tokens of a user
func UserTokens(userID string) ([]*Token, error) {
	return getTokens(userID)
}
