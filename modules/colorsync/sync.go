package colorsync

import (
	"gitlab.com/MrGunflame/robbot/bot"
)

func sync(ctx *bot.Context) error {
	msg, err := ctx.Respond("**Starting synchronizing task now...**")
	if err != nil {
		return err
	}

	if err := refreshTask(ctx.Session, ctx.Event.GuildID); err != nil {
		ctx.Session.Bare().ChannelMessageEdit(msg.ChannelID, msg.ID, ":x: **Task failed**")
		return err
	}

	ctx.Session.Bare().ChannelMessageEdit(msg.ChannelID, msg.ID, ":white_check_mark: **Task finished successfully**")
	return nil
}
