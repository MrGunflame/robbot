package colorsync

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
)

func set(ctx *bot.Context) error {
	// Exactly 2 args must be given
	ch := ctx.Arguments.PopFirst()
	color := ctx.Arguments.PopFirst().String()
	if ch.String() == "" || color == "" {
		return ctx.InvalidCommandUsage()
	}

	// Extract channel id
	chID, err := getChannel(ctx.Session, ctx.Event.GuildID, ch)
	if err != nil {
		return err
	} else if chID == "" {
		ctx.Failure(fmt.Sprintf("Cannot find channel `%s`.", ch.String()))
		return nil
	}

	// Insert into database
	if _, err := newChannelColorLink(chID, ctx.Event.GuildID, color).insert(); err != nil {
		return err
	}

	ctx.Success(fmt.Sprintf("Successfully linked `%s` with color `%s`.", ch.String(), color))
	return nil
}
