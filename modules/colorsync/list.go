package colorsync

import (
	"strings"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func list(ctx *bot.Context) error {
	links, err := getChannelColorLinks(ctx.Event.GuildID)
	if err != nil {
		return err
	}

	var b strings.Builder
	for _, l := range links {
		for _, s := range []string{util.MentionChannel(l.channel), ": ", l.color, "\n"} {
			b.WriteString(s)
		}
	}

	ctx.RespondEmbed(&discord.Embed{
		Title:       "Linked Channels",
		Description: b.String(),
	})
	return nil
}
