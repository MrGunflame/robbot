package colorsync

import (
	"gitlab.com/MrGunflame/robbot/store"
)

type channelColorLink struct {
	id      int
	channel string
	guild   string // Discord Guild ID
	color   string
}

func newChannelColorLink(channel, guild, color string) *channelColorLink {
	return &channelColorLink{
		channel: channel,
		guild:   guild,
		color:   color,
	}
}

func (d *channelColorLink) insert() (int, error) {
	return store.Insert("INSERT INTO colorsync (channel, guild, color) VALUES (?, ?, ?)", d.channel, d.guild, d.color)
}

func getChannelColorLinks(guild string) ([]*channelColorLink, error) {
	rows, err := store.Select("SELECT id, channel, color FROM colorsync WHERE guild = ?", guild)
	if err != nil {
		return nil, err
	}

	var links []*channelColorLink
	for rows.Next() {
		var l channelColorLink
		if err := rows.Scan(&l.id, &l.channel, &l.color); err != nil {
			return nil, err
		}
		links = append(links, &l)
	}

	return links, nil
}

func deleteChannelColorLink(channel string) (int, error) {
	return store.Delete("DELETE FROM colorsync WHERE guild = ?", channel)
}
