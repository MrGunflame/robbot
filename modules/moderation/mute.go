package moderation

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func mute(ctx *bot.Context) error {
	// Argument 1: UserID
	user, ok := ctx.Arguments.PopFirst().UserID()
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	// Argument 2: Duration (optional)
	var duration time.Duration
	if dur := ctx.Arguments.PopFirst().String(); len(dur) != 0 {
		var err error
		duration, err = time.ParseDuration(dur)
		if err != nil {
			return ctx.InvalidCommandUsage()
		}
	}

	roleID, err := getMutedRole(ctx.Event.GuildID)
	if err != nil {
		return err
	} else if roleID == "" {
		// Create a new role
		role, err := ctx.Session.Bare().GuildRoleCreate(ctx.Event.GuildID)
		if err != nil {
			return err
		}

		// Set the role details
		if _, err := ctx.Session.Bare().GuildRoleEdit(ctx.Event.GuildID, role.ID, "Muted", role.Color, role.Hoist, 0, role.Mentionable); err != nil {
			return err
		}

		// Insert into database
		if _, err := newMutedRole(ctx.Event.GuildID, role.ID).insert(); err != nil {
			return err
		}

		roleID = role.ID
	}

	channels, err := ctx.Session.Bare().GuildChannels(ctx.Event.GuildID)
	if err != nil {
		return err
	}

	// Apply all missing permissions
	const permissionValue = discordgo.PermissionSendMessages + discordgo.PermissionVoiceSpeak

	for _, c := range channels {
		found, changed := false, false
		for _, po := range c.PermissionOverwrites {
			if po.ID == roleID {
				found = true
				if po.Deny != permissionValue {
					po.Deny = permissionValue
					changed = true
				}
			}
		}

		// Add the muted role to the overwrites
		if !found {
			c.PermissionOverwrites = append(c.PermissionOverwrites, &discordgo.PermissionOverwrite{
				Type: "role",
				ID:   roleID,
				Deny: permissionValue,
			})
			changed = true
		}

		// Only send a request if any changes were made
		if changed {
			if _, err := ctx.Session.Bare().ChannelEditComplex(c.ID, &discordgo.ChannelEdit{
				PermissionOverwrites: c.PermissionOverwrites,
			}); err != nil {
				return err
				// continue
			}
		}
	}

	// Insert into database
	if _, err := newMutedUser(ctx.Event.GuildID, user).insert(); err != nil {
		return err
	}

	go func() {
		time.Sleep(duration)
		removeMutedUser(ctx.Event.GuildID, user)
		ctx.Session.GuildMemberRoleRemove(ctx.Event.GuildID, user, roleID)
	}()

	// Add the role
	if err := ctx.Session.GuildMemberRoleAdd(ctx.Event.GuildID, user, roleID); err != nil {
		return err
	}

	ctx.Success(fmt.Sprintf("Muted %s.", util.MentionUser(user)))
	return nil
}
