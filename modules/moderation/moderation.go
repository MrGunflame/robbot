package moderation

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/store"
)

const (
	permissionBan  = "ban"
	permissionKick = "kick"
	permissionMute = "mute"
)

// Module moderation provides moderation commands and utils
var Module = &bot.Module{
	Name:        "moderation",
	Description: "",
	GuildOnly:   true,
	Commands: map[string]*bot.Command{
		"ban": {},
		"kick": {
			Description: "Kick a user from the server with an optional warning message.",
			Usage:       "<User> [Message]",
			Example:     "@Robbbbbbb \"Get out!\"",
			Permissions: []string{permissionKick},
			Execute:     kick,
		},
		"mute": {
			Description: "Mutes a user, making unable to speak or send messages.",
			Usage:       "<User> [Duration]",
			Example:     "@Robbbbbbb 1h",
			Permissions: []string{permissionMute},
			Execute:     mute,
		},
		"unmute": {
			Description: "Unmute a user.",
			Usage:       "<User>",
			Example:     "@Robbbbbbb",
			Permissions: []string{permissionMute},
			Execute:     unmute,
		},
	},
	Store: &store.Store{
		Tables: []*store.Table{
			{
				Name: "moderation_muted_roles",
				Columns: []*store.Column{
					{
						Name:  "guild",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull, store.SQLFlagUnique},
					},
					{
						Name:  "role",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
			{
				Name: "moderation_muted_users",
				Columns: []*store.Column{
					{
						Name:  "guild",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "user",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "until",
						Type:  "BIGINT",
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
		},
	},
}
