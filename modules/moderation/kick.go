package moderation

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func kick(ctx *bot.Context) error {
	userID, ok := ctx.Arguments.PopFirst().UserID()
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	message := ctx.Arguments.PopFirst().String()

	// Open a DM channel, get the guild name and send the message to the user
	if ch, err := ctx.Session.UserChannelCreate(userID); err == nil {
		if guild, err := ctx.Session.Guild(ctx.Event.GuildID); err == nil {
			ctx.Session.ChannelMessageSend(ch.ID, fmt.Sprintf("You were kicked from **%s**:\n%s", guild.Name, message))
		}
	}

	if err := ctx.Session.GuildMemberKick(ctx.Event.GuildID, userID); err != nil {
		return err
	}

	ctx.Success(fmt.Sprintf("Kicked %s.", util.MentionUser(userID)))
	return nil
}
