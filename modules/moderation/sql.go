package moderation

import (
	"time"

	"gitlab.com/MrGunflame/robbot/store"
)

func newMutedRole(guildID, roleID string) *mutedRole {
	return &mutedRole{
		guild: guildID,
		role:  roleID,
	}
}

type mutedRole struct {
	guild string
	role  string
}

func (d *mutedRole) insert() (int, error) {
	return store.Insert("INSERT INTO moderation_muted_roles (guild, role) VALUES (?, ?)", d.guild, d.role)
}

func getMutedRole(guildID string) (string, error) {
	rows, err := store.Select("SELECT role FROM moderation_muted_roles WHERE guild = ?", guildID)
	if err != nil {
		return "", err
	}

	var role string
	for rows.Next() {
		if err := rows.Scan(&role); err != nil {
			return "", err
		}
	}

	return role, nil
}

func newMutedUser(guildID, userID string) *mutedUser {
	return &mutedUser{
		guild: guildID,
		user:  userID,
	}
}

type mutedUser struct {
	guild    string
	user     string
	duration time.Duration
}

func (d *mutedUser) insert() (int, error) {
	return store.Insert("INSERT INTO moderation_muted_users (guild, user, until) VALUES (?, ?, ?)", d.guild, d.user, time.Now().Add(d.duration).Unix())
}

func removeMutedUser(guildID, userID string) (int, error) {
	return store.Delete("DELETE FROM moderation_muted_users WHERE guild = ? AND user = ?", guildID, userID)
}
