package moderation

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func unmute(ctx *bot.Context) error {
	// Argument 1: UserID
	userID, ok := ctx.Arguments.PopFirst().UserID()
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	// Get the muted role from cache or from database
	roleID, err := getMutedRole(ctx.Event.GuildID)
	if err != nil {
		return err
	} else if roleID == "" {
		return nil
	}

	if err := ctx.Session.GuildMemberRoleRemove(ctx.Event.GuildID, userID, roleID); err != nil {
		return err
	}

	ctx.Success(fmt.Sprintf("Unmuted %s.", util.MentionUser(userID)))
	return nil
}
