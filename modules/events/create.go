package events

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
)

// date time [repeat]
func create(ctx *bot.Context) error {
	// Argument 1: Title
	title := ctx.Arguments.PopFirst().String()
	if len(title) <= 0 {
		return ctx.InvalidCommandUsage()
	} else if len(title) > 64 {
		ctx.Failure("Maximum length for an event title is 64 characters.")
		return nil
	}

	// Argument 2: Date
	date := ctx.Arguments.PopFirst().String()

	// Argument 3: Time
	t := ctx.Arguments.PopFirst().String()

	// Argument 4: Repeat timing [OPTIONAL]
	repeat := ctx.Arguments.PopFirst().String()

	// Argument 5: Description [OPTIONAL]
	desc := ctx.Arguments.PopFirst().String()

	// Load the default time values
	// YEAR		=> Current year
	// MONTH	=> Current month
	// DAY		=> Current day
	// HOUR		=> ALWAYS REQUIRED
	// MINUTE	=> 0
	// SECOND	=> 0
	n := time.Now().UTC()
	year, month, day, hour, min, sec := n.Year(), n.Month(), n.Day(), n.Hour(), 0, 0

	// Date in the format dd-mm-[yyyy]
	// date := ctx.Arguments.PopFirst().String()
	if date != "" {
		sl := strings.Split(date, "-")

		for i, s := range sl {
			// Convert to int
			n, err := strconv.Atoi(s)
			if err != nil {
				return err
			}

			switch i {
			case 0: // DAY
				day = n
			case 1: // MONTH
				month = time.Month(n)
			case 2: // YEAR
				year = n
			}
		}
	} else {
		return ctx.InvalidCommandUsage()
	}

	// Time in the format of hh:mm:[ss]
	// t := ctx.Arguments.PopFirst().String()
	if t != "" {
		sl := strings.Split(t, ":")

		for i, s := range sl {
			n, err := strconv.Atoi(s)
			if err != nil {
				return err
			}

			switch i {
			case 0: // HOUR
				hour = n
			case 1: // MINUTE
				min = n
			case 2: // SECOND
				sec = n
			}
		}
	}

	// timeUnixRepeat is the interval in seconds between the new event being scheduled
	var timeUnixRepeat int64
	if repeat != "" {
		switch strings.ToLower(repeat) {
		case "hour", "hourly":
			timeUnixRepeat = 3600 // 60 * 60
		case "day", "daily":
			timeUnixRepeat = 68400 // 24 * 60 * 60
		case "week", "weekly":
			timeUnixRepeat = 604800 // 7 * 24 * 60 * 60
		default:
			// Parse the duration
			dur, err := time.ParseDuration(repeat)
			if err != nil {
				ctx.Failure(fmt.Sprintf("Unkown repeat interval: `%s`.", repeat))
				return nil
			}

			// Convert to int64, round down to seconds
			n := int64(dur.Seconds())
			if n < 0 {
				ctx.Failure(fmt.Sprintf("Negative repeat intervals are not allowed."))
				return nil
			}

			timeUnixRepeat = n
		}
	}

	// Insert into database
	if _, err := newEvent(ctx.Event.GuildID, title, desc, time.Date(year, month, day, hour, min, sec, 0, time.UTC).Unix(), timeUnixRepeat).insert(); err != nil {
		return err
	}

	ctx.Success("Successfully created event.")
	return nil
}
