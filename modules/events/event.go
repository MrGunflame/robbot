package events

import (
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/store"
)

var (
	permissionManage = "manage"
	permissionAdmin  = "admin"
)

// Module event schedules events
var Module = &bot.Module{
	Name:        "events",
	Description: "Create and manage events from within Discord. Schedule one-time or repeated events and track participation.",
	GuildOnly:   true,
	Commands: map[string]*bot.Command{
		"announce": {
			Description: "Manually check all events and announce close events.",
			Usage:       "",
			Example:     "",
			Permissions: []string{permissionAdmin, permissionManage},
			Execute:     announce,
		},
		"create": {
			Description: "Create a new event. The Hour is always required. If no date is given, the current day is taken as a default. If no minutes and seconds are given 0 is used by default. UTC is used when no other timezone is given.",
			Usage:       "<Title> <DD[-MM[-YYYY]]> <hh[:mm[:ss]]> [Repeat-Interval] [Description]",
			Example:     "21-01-2019 07:00",
			Permissions: []string{permissionManage},
			Execute:     create,
		},
		"delete": {
			Description: "Delete an event.",
			Usage:       "<Event-ID>",
			Example:     "1",
			Permissions: []string{permissionManage},
			Execute:     delete,
		},
		"channel": {
			Description: "Show or set a channel to announce events in.",
			Usage:       "",
			Example:     "",
			Permissions: []string{permissionAdmin, permissionManage},
			Execute:     channel,
		},
		"list": {
			Description: "List all scheduled events.",
			Usage:       "",
			Example:     "",
			Permissions: []string{permissionManage},
			Execute:     list,
		},
	},
	Permissions: []string{permissionAdmin, permissionManage},
	Tasks: []*bot.Task{
		{
			Name:     "announce",
			Interval: 1 * time.Hour,
			Execute:  announceTask,
		},
	},
	Store: &store.Store{
		Tables: []*store.Table{
			{
				Name: "events_channels",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "guild",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull, store.SQLFlagPrimaryKey},
					},
				},
			},
			{
				Name: "events_events",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "INT",
						Flags: []store.SQLFlag{store.SQLFlagAutoIncrement, store.SQLFlagNotNull, store.SQLFlagPrimaryKey, store.SQLFlagUnsigned},
					},
					{
						Name:  "guild",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "title",
						Type:  "VARCHAR",
						Len:   64,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "text",
						Type:  "TINYTEXT",
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "time_unix",
						Type:  "BIGINT", // int64
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "time_unix_repeat",
						Type:  "BIGINT", // int64
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "last_announce",
						Type:  "BIGINT", // int64
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
		},
	},
}
