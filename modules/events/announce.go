package events

import "gitlab.com/MrGunflame/robbot/bot"

func announce(ctx *bot.Context) error {
	ctx.RunTask(announceTask)
	return nil
}
