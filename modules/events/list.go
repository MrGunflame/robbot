package events

import (
	"strconv"
	"strings"
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
)

func list(ctx *bot.Context) error {
	// Get all events from database
	events, err := getEvents(ctx.Event.GuildID)
	if err != nil {
		return err
	}

	var b strings.Builder
	for _, e := range events {
		for _, s := range []string{"**[", strconv.Itoa(e.id), "]: ", e.title, "**\n", discord.FormatTimeDefault(time.Unix(e.timeUnix, 0).UTC()), "\n"} {
			b.WriteString(s)
		}
	}

	ctx.RespondEmbed(&discord.Embed{
		Title:       "Events",
		Description: b.String(),
		Footer:      discord.DefaultEmbedFooter(""),
	})
	return nil
}
