package events

import (
	"gitlab.com/MrGunflame/robbot/store"
)

// Update the announce channel of a guild
func setChannel(guild, id string) (int, error) {
	return store.Insert("INSERT INTO events_channels (id, guild) VALUES (?, ?) ON DUPLICATE KEY UPDATE id = id, guild = guild", id, guild)
}

// Return the channel id of the guilds announce channel or empty string if not set
func getChannel(guild string) (string, error) {
	rows, err := store.Select("SELECT id FROM events_channels WHERE guild = ?", guild)
	if err != nil {
		return "", err
	}

	var chID string
	for rows.Next() {
		if err := rows.Scan(&chID); err != nil {
			return "", err
		}
	}

	return chID, nil
}

func newEvent(guild, title, text string, timeUnix, repeat int64) *event {
	return &event{
		guild:        guild,
		title:        title,
		text:         text,
		timeUnix:     timeUnix,
		repeat:       repeat,
		lastAnnounce: 0,
	}
}

type event struct {
	id           int
	guild        string
	title        string
	text         string
	timeUnix     int64
	repeat       int64 // wait time between each event
	lastAnnounce int64 // last announcement sent to prevent spamming if the event was posted already or the bot crashes
}

func (d *event) insert() (int, error) {
	return store.Insert("INSERT INTO events_events (guild, title, text, time_unix, time_unix_repeat, last_announce) VALUES (?, ?, ?, ?, ?, 0)", d.guild, d.title, d.text, d.timeUnix, d.repeat)
}

func (d *event) update() (int, error) {
	return store.Insert("INSERT INTO events_events (id, guild, title, text, time_unix, time_unix_repeat, last_announce) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE guild = VALUES(guild), title = VALUES(title), text = VALUES(text), time_unix = VALUES(time_unix), time_unix_repeat = VALUES(time_unix_repeat), last_announce = VALUES(last_announce)", d.id, d.guild, d.title, d.text, d.timeUnix, d.repeat, d.lastAnnounce)
}

func getEvents(guild string) ([]*event, error) {
	rows, err := store.Select("SELECT id, title, text, time_unix, time_unix_repeat, last_announce FROM events_events WHERE guild = ?", guild)
	if err != nil {
		return nil, err
	}

	var events []*event
	for rows.Next() {
		e := event{guild: guild}
		if err := rows.Scan(&e.id, &e.title, &e.text, &e.timeUnix, &e.repeat, &e.lastAnnounce); err != nil {
			return nil, err
		}
		events = append(events, &e)
	}

	return events, nil
}

func deleteEvent(id int, guild string) (int, error) {
	return store.Delete("DELETE FROM events_events WHERE id = ? AND guild = ?", id, guild)
}
