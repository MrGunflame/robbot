package events

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func channel(ctx *bot.Context) error {
	// chID, ok := ctx.Arguments.PopFirst().ChannelID()
	// if !ok {
	// 	ctx.InvalidCommandUsage()
	// }

	// Argument 1: Operation
	switch ctx.Arguments.PopFirst().String() {
	case "set":
		// Argument 2: Channel ID
		chID, ok := ctx.Arguments.PopFirst().ChannelID()
		if !ok {
			ctx.InvalidCommandUsage()
		}

		// Update database
		if _, err := setChannel(ctx.Event.GuildID, chID); err != nil {
			return err
		}

		ctx.Success(fmt.Sprintf("Successfully set announcement channel to %s.", util.MentionChannel(chID)))
		return nil
	}

	// Get channel from database
	chID, err := getChannel(ctx.Event.GuildID)
	if err != nil {
		return err
	}

	ctx.Respond(fmt.Sprintf("Announcement channel: %s", util.MentionChannel(chID)))
	return nil
}
