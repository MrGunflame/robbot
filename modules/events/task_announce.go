package events

import (
	"fmt"
	"time"

	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func announceTask(s *discord.Session, guild string) error {
	// Get all events from database
	// Return when no events are present
	events, err := getEvents(guild)
	if err != nil {
		return err
	} else if len(events) == 0 {
		return nil
	}

	// Get the guilds announce channel
	// Return when no channel is set
	channel, err := getChannel(guild)
	if err != nil {
		return err
	} else if channel == "" {
		return nil
	}

	// Get current time and post all events within the next week
	// Weektime: 60 * 60 * 24 * 7 * time.Second = 604800 * time.Second
	now := time.Now().UTC().Unix()
	for _, e := range events {
		if now >= (e.timeUnix-608400) && (now-600) >= e.lastAnnounce {
			announceEvent(s, e, channel)

			// Update the lastAnnounce field
			e.lastAnnounce = now

			// Schedule the next run or delete when onetime event
			if e.repeat > 0 {
				e.timeUnix += e.repeat
			} else {
				// Delete from database
				if _, err := deleteEvent(e.id, e.guild); err != nil {
					return err
				}
				continue
			}

			if _, err := e.update(); err != nil {
				return err
			}
		}
	}

	return nil
}

func announceEvent(s *discord.Session, e *event, channel string) error {

	msg, err := s.ChannelMessageSendEmbed(channel, &discord.Embed{
		Title:       util.ConcatStrings("Event: ", e.title),
		Color:       util.ColorOrange,
		Description: fmt.Sprintf("%s\n\n**When**: %s\n**Can you participate?**\n:white_check_mark:: Yes\n:x:: No", e.text, time.Unix(e.timeUnix, 0).Format("Monday, 02. Jan 2006 15:04 MST")),
	})
	if err != nil {
		return err
	}

	for _, r := range []string{"✅", "❌"} {
		s.Bare().MessageReactionAdd(msg.ChannelID, msg.ID, r)
	}

	return nil
}
