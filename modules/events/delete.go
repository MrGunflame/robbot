package events

import "gitlab.com/MrGunflame/robbot/bot"

func delete(ctx *bot.Context) error {
	// Argument 1: Event ID
	id, ok := ctx.Arguments.PopFirst().Int()
	if !ok {
		ctx.InvalidCommandUsage()
	}

	// Delete from database
	if _, err := deleteEvent(id, ctx.Event.GuildID); err != nil {
		return err
	}

	ctx.Success("Successfully deleted event.")
	return nil
}
