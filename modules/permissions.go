package modules

import (
	"sort"

	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules/permissions"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func init() {
	for _, m := range modules {
		for _, s := range m.Permissions {
			permissions.AllPermissions = append(permissions.AllPermissions, util.ConcatStrings(m.Name, ".", s))
		}
	}
	// Sort the slice for quicker access times
	sort.Sort(permissions.AllPermissions)
	permissions.AllPermissionsLen = permissions.AllPermissions.Len()
}

func hasPermission(s *discord.Session, m *discord.MessageCreate, mod string, req []string) (bool, error) {
	guild, err := s.Bare().Guild(m.GuildID)
	if err != nil {
		return false, err
	}

	var nodes []string
	for _, n := range req {
		nodes = append(nodes, util.ConcatStrings(mod, ".", n))
	}

	return permissions.HasPermission(guild.OwnerID, m.Author.ID, guild.ID, m.Member.Roles, nodes)
}
