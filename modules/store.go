package modules

import "gitlab.com/MrGunflame/robbot/store"

// InitStore calls store.InitSQLTables with all registered modules
func InitStore() error {
	var tables [][]*store.Table
	for _, m := range modules {
		if m.Store != nil {
			tables = append(tables, m.Store.Tables)
		}
	}

	return store.InitSQLTables(tables)
}
