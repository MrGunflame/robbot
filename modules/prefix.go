package modules

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules/permissions"
)

// Prefix contains the prefix for each server
var Prefix = make(map[string]string)

// The buildtin command [!]prefix
func prefix(s *discord.Session, m *discord.MessageCreate, args *bot.Arguments) error {
	// Show the prefix
	if args.Len() <= 0 {
		pref, ok := Prefix[m.GuildID]
		if !ok {
			pref = "!"
		}
		s.Bare().ChannelMessageSend(m.ChannelID, fmt.Sprintf("The Prefix for this server is `%s`.", pref))
		return nil
	}

	// Check if the author is bot owner or server owner
	if guild, err := s.Bare().Guild(m.GuildID); m.Author.ID != permissions.BotOwner && err != nil && guild.OwnerID != m.Author.ID {
		s.Bare().ChannelMessageSend(m.ChannelID, ":no_entry_sign: Only the bot or server owner can change the server prefix.")
		return nil
	}

	// Set the first argument as prefix
	Prefix[m.GuildID] = args.Get(0).String()
	s.Bare().ChannelMessageSend(m.ChannelID, fmt.Sprintf("Successfully updated server prefix to `%s`.", args.Get(0).String()))
	return nil
}
