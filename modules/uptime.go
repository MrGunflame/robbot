package modules

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"

	"github.com/bwmarrin/discordgo"
)

// startTime to determite the process runtime
var startTime time.Time

func init() {
	startTime = time.Now()
}

// The builtin command [!]uptime
func uptime(s *discord.Session, m *discord.MessageCreate, _ *bot.Arguments) error {
	d := time.Since(startTime)
	s.Bare().ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
		Title:       "Uptime",
		Color:       util.ColorRed,
		Description: fmt.Sprintf("%d hrs, %d min, %d sec", int(d.Hours()), int(d.Minutes())-int(math.Floor(d.Hours())*60), int(d.Seconds())-int(math.Floor(d.Minutes())*60)),
	})
	return nil
}
