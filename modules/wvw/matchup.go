package wvw

import (
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/MrGunflame/gw2api"
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

// Refresh the api only every 300 seconds
var (
	matchupCache = util.NewCache(300 * time.Second)
	worldCache   = util.NewCache(300 * time.Second)
)

var (
	exp = regexp.MustCompile(`^(t[1-5])$`)
)

func matchup(ctx *bot.Context) error {
	ctx.Typing()

	// discordFlag returns the national flag based on a world name and a match id
	// discordFlag := func(id, name string) string {
	// 	// NA
	// 	if strings.HasPrefix(id, "1-") {
	// 		return ":flag_us:"
	// 	}

	// 	// EU
	// 	switch name[len(name)-5 : len(name)-1] {
	// 	case "[DE]":
	// 		return ":flag_de:"
	// 	case "[FR]":
	// 		return ":flag_fr:"
	// 	case "[SP]":
	// 		return ":flag_es:"
	// 	}

	// 	return ":flag_eu:"
	// }

	a1 := ctx.Arguments.PopFirst().String()
	switch a1 {
	// Current Match Overview
	case "":
		// Try to serve from cache
		if val, ok := matchupCache.Get("all"); ok {
			ctx.RespondEmbed(val.(*discord.Embed))
			return nil
		}

		// Fetch all matches
		matches, err := gw2api.New().WvWMatches()
		if err != nil {
			return err
		}
		matches = extractRegion("eu", matches)

		embed, err := matchOverview(matches)
		if err != nil {
			return err
		}

		// Insert into cache
		matchupCache.Insert("all", embed)
		ctx.RespondEmbed(embed)
		return nil
	// Predict Next Weeks Matches
	case "next":
		if val, ok := matchupCache.Get("next"); ok {
			ctx.RespondEmbed(val.(*discord.Embed))
			return nil
		}

		// Fetch all matches
		matches, err := gw2api.New().WvWMatches()
		if err != nil {
			return err
		}
		matches = extractRegion("eu", matches)
		matches = predictMatches(matches)

		embed, err := matchOverview(matches)
		if err != nil {
			return err
		}

		// Insert into cache
		matchupCache.Insert("next", embed)
		ctx.RespondEmbed(embed)
		return nil
	}

	var match *gw2api.WvWMatch

	// Match by tier
	if exp.MatchString(a1) {
		a1 = strings.Replace(a1, "t", "", 1)

		// Use the match from cache when avaliable
		if val, ok := matchupCache.Get(util.ConcatStrings("2-", a1)); ok {
			ctx.ReplyEmbed(val.(*discord.Embed))
			return nil
		}

		matches, err := gw2api.New().WvWMatches(util.ConcatStrings("2-", a1))
		if err != nil {
			return err
		}

		for _, m := range matches {
			match = m
			break
		}
	} else {
		// Expect a1 to be a world
		worlds, err := gw2api.New().Worlds()
		if err != nil {
			return err
		}

		for _, w := range worlds {
			if w.Name == a1 {
				var err error
				match, err = gw2api.New().WvWMatchByWorldID(w.ID)
				if err != nil {
					return err
				}
				break
			}
		}
	}

	// Generate match region and tier
	var region, tier string
	sl := strings.Split(match.ID, "-")
	for i, v := range sl {
		switch i {
		case 0:
			switch v {
			case "1":
				region = "NA"
			case "2":
				region = "EU"
			}
		case 1:
			tier = v
		}
	}

	embed := &discord.Embed{
		Title:  util.ConcatStrings("WvW Match: ", region, " Tier ", tier),
		Color:  util.ColorOrange,
		Footer: discord.DefaultEmbedFooter(util.ConcatStrings("Updated: ", time.Now().UTC().Format("2006-01-02 15:04:05 MST"))),
	}

	// The keys returned by the api are not guaranteed to be in order
	// Display them in the order [green, blue, red]
	keys := []string{"green", "blue", "red"}

	// Collect all world ids in the match
	var worldIDs []int
	for _, v := range match.AllWorlds {
		for _, w := range v {
			worldIDs = append(worldIDs, w)
		}
	}

	// Fetch all world ids required from the api
	worlds, err := gw2api.New().Worlds(worldIDs...)
	if err != nil {
		return err
	}

	// Map all world ids to their names
	worldMap := make(map[int]string)
	for _, w := range worlds {
		worldMap[w.ID] = w.Name
	}

	// Each function represents a field displayed for each side participating in the match
	fieldFuncs := []func(k string) (string, string){
		func(k string) (string, string) {
			// lang returns the national abbrevitation for a world
			lang := func(w string) string {
				// Check for a language suffix at the end of the world name
				for _, p := range []string{"de", "fr", "sp"} {
					if strings.HasSuffix(w, util.ConcatStrings("[", strings.ToUpper(p), "]")) {
						return p
					}
				}

				// Matches starting with id 1-.. are na matches
				if strings.HasPrefix(match.ID, "1-") {
					return "us"
				}

				return "eu"
			}

			// Collect all names of linked worlds
			var links []string
			if len(match.AllWorlds[k]) > 1 {
				for _, w := range match.AllWorlds[k] {
					if w != match.Worlds[k] {
						links = append(links, util.ConcatStrings("+", worldMap[w]))
					}
				}
			}

			link := "No Link"
			if len(links) > 0 {
				link = strings.Join(links, "\n")
			}

			return util.ConcatStrings("**:flag_", lang(worldMap[match.Worlds[k]]), ": ", worldMap[match.Worlds[k]], "**"), link
		},
		func(k string) (string, string) {
			return "Kills:", strconv.Itoa(match.Kills[k])
		},
		func(k string) (string, string) {
			return "Deaths:", strconv.Itoa(match.Deaths[k])
		},
		func(k string) (string, string) {
			var kd string
			if match.Deaths[k] < 1 {
				kd = strconv.Itoa(match.Kills[k])
			} else {
				kd = strconv.FormatFloat(float64(match.Kills[k])/float64(match.Deaths[k]), 'f', 4, 64)
			}
			return "K/D:", kd
		},
		func(k string) (string, string) {
			return "VP:", strconv.Itoa(match.VictoryPoints[k])
		},
	}

	for _, f := range fieldFuncs {
		for _, k := range keys {
			name, value := f(k)
			embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
				Name:   name,
				Value:  value,
				Inline: true,
			})
		}
	}

	// Put the match into the cache
	matchupCache.Insert(match.ID, embed)
	ctx.RespondEmbed(embed)
	return nil
}

func matchOverview(matches []*gw2api.WvWMatch) (*discord.Embed, error) {
	// Collect all world ids
	var worldIDs []int
	for _, m := range matches {
		for _, v := range m.AllWorlds {
			for _, w := range v {
				worldIDs = append(worldIDs, w)
			}
		}
	}

	worlds, err := gw2api.New().Worlds(worldIDs...)
	if err != nil {
		return nil, err
	}

	worldMap := make(map[int]string)
	for _, w := range worlds {
		worldMap[w.ID] = w.Name
	}

	// Links contains all world links for all matches for all colors
	links := make([]map[string]string, len(matches))
	for i, m := range matches {
		links[i] = make(map[string]string)
		for k, v := range m.AllWorlds {
			var b strings.Builder
			for _, w := range v {
				if w != m.Worlds[k] {
					for _, s := range []string{"+", worldMap[w], "\n"} {
						b.WriteString(s)
					}
				}
			}

			if b.Len() > 0 {
				links[i][k] = b.String()
			} else {
				links[i][k] = "No Link"
			}
		}
	}

	keys := []string{"green", "blue", "red"}

	embed := &discord.Embed{
		Title:  "EU WvW Matches",
		Color:  util.ColorOrange,
		Footer: discord.DefaultEmbedFooter(util.ConcatStrings("Updated ", discord.FormatTimeDefault(time.Now().UTC()))),
	}

	for i, m := range matches {
		// Exclude NA matches
		if strings.HasPrefix(m.ID, "2-") {
			for _, k := range keys {
				embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
					Name:   util.ConcatStrings("**", discordFlag(m.ID, worldMap[m.Worlds[k]]), " ", worldMap[m.Worlds[k]], "**"),
					Value:  links[i][k],
					Inline: true,
				})
			}
		}
	}

	return embed, nil
}

func discordFlag(id string, name string) string {
	// NA
	if strings.HasPrefix(id, "1-") {
		return ":flag_us:"
	}

	// EU
	switch name[len(name)-4 : len(name)] {
	case "[DE]":
		return ":flag_de:"
	case "[FR]":
		return ":flag_fr:"
	case "[SP]":
		return ":flag_es:"
	}

	return ":flag_eu:"
}

func extractRegion(region string, matches []*gw2api.WvWMatch) []*gw2api.WvWMatch {
	var new []*gw2api.WvWMatch
	for _, m := range matches {
		switch region {
		case "eu":
			if strings.HasPrefix(m.ID, "2-") {
				new = append(new, m)
			}
		}
	}

	return new
}
