package wvw

import (
	"fmt"
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/MrGunflame/gw2api"
)

var populationCache = util.NewCache(300 * time.Second)

func population(ctx *bot.Context) error {
	ctx.Typing()

	// Try to server from cache
	if ctx.Arguments.Len() == 0 {
		if val, ok := populationCache.Get("all"); ok {
			ctx.RespondEmbed(val.(*discord.Embed))
			return nil
		}
	} else {
		if val, ok := populationCache.Get(ctx.Arguments.First().String()); ok {
			ctx.RespondEmbed(val.(*discord.Embed))
			return nil
		}
	}

	// Fetch all worlds
	worlds, err := gw2api.New().Worlds()
	if err != nil {
		return err
	}

	embed := &discord.Embed{
		Title:  "World Population",
		Color:  util.ColorOrange,
		Fields: []*discordgo.MessageEmbedField{},
	}

	// Display all worlds
	if !ctx.Arguments.Next() {
		for _, w := range worlds {
			embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
				Name:   w.Name,
				Value:  w.Population,
				Inline: true,
			})
		}

		// Insert into cache
		populationCache.Insert("all", embed)
		ctx.RespondEmbed(embed)
		return nil
	}

	// Display a specific world
	name := ctx.Arguments.PopFirst().String()
	for _, w := range worlds {
		if w.Name == name {
			embed.Title = util.ConcatStrings(embed.Title, ": ", w.Name)
			embed.Description = util.ConcatStrings("**Current**: ", w.Population)
			switch w.Population {
			case "Full":
				embed.Color = util.ColorRed
			case "VeryHigh":
				embed.Color = util.ColorOrange
			case "High":
				embed.Color = util.ColorYellow
			case "Medium":
				embed.Color = util.ColorGreen
			}

			// Insert into cache
			populationCache.Insert(name, embed)
			ctx.RespondEmbed(embed)
			return nil
		}
	}

	ctx.Failure(fmt.Sprintf("Cannot find world with name `%s`.", name))
	return nil
}
