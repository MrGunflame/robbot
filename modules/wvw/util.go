package wvw

import (
	"gitlab.com/MrGunflame/gw2api"
)

func predictMatches(matches []*gw2api.WvWMatch) []*gw2api.WvWMatch {
	// Make a new slice with same len
	var new []*gw2api.WvWMatch
	for _, m := range matches {
		new = append(new, &gw2api.WvWMatch{
			ID: m.ID,
			Worlds: func() map[string]int {
				mp := make(map[string]int)
				for k, v := range m.Worlds {
					mp[k] = v
				}
				return mp
			}(),
			AllWorlds: func() map[string][]int {
				mp := make(map[string][]int)
				for k, v := range m.AllWorlds {
					mp[k] = v
				}
				return mp
			}(),
		})
	}

	for _, m := range matches {
		for c := range m.Worlds {
			if m.VictoryPoints[c] > m.VictoryPoints["red"] {
				m.Worlds[c], m.Worlds["red"] = m.Worlds["red"], m.Worlds[c]
				m.AllWorlds[c], m.AllWorlds["red"] = m.AllWorlds["red"], m.AllWorlds[c]
				m.VictoryPoints[c], m.VictoryPoints["red"] = m.VictoryPoints["red"], m.VictoryPoints[c]
			}

			if m.VictoryPoints[c] < m.VictoryPoints["green"] {
				m.Worlds[c], m.Worlds["green"] = m.Worlds["green"], m.Worlds[c]
				m.AllWorlds[c], m.AllWorlds["green"] = m.AllWorlds["green"], m.AllWorlds[c]
				m.VictoryPoints[c], m.VictoryPoints["green"] = m.VictoryPoints["green"], m.VictoryPoints[c]
			}
		}
	}

	for i, m := range matches {
		switch i {
		case 0:
			new[i].Worlds["green"] = m.Worlds["red"]
			new[i+1].Worlds["green"] = m.Worlds["green"]

			new[i].AllWorlds["green"] = m.AllWorlds["red"]
			new[i+1].AllWorlds["green"] = m.AllWorlds["green"]
		case len(matches) - 1:
			new[i-1].Worlds["red"] = m.Worlds["red"]
			new[i].Worlds["red"] = m.Worlds["green"]

			new[i-1].AllWorlds["red"] = m.AllWorlds["red"]
			new[i].AllWorlds["red"] = m.AllWorlds["green"]
		default:
			new[i-1].Worlds["red"] = m.Worlds["red"]
			new[i+1].Worlds["green"] = m.Worlds["green"]

			new[i-1].AllWorlds["red"] = m.AllWorlds["red"]
			new[i+1].AllWorlds["green"] = m.AllWorlds["green"]
		}
	}

	return new
}
