package wvw

import (
	"gitlab.com/MrGunflame/robbot/bot"
)

// Module WvW gives access to Guild Wars 2 WvW Data
var Module = &bot.Module{
	Name:        "wvw",
	Description: "Access Guild Wars 2 WvW data",
	GuildOnly:   false,
	Commands: map[string]*bot.Command{
		"matchup": {
			Description: "View the current matches",
			Usage:       "[World] / [t1-5] / next",
			Example:     "\"Whiteside Ridge\" | t1 | next",
			Execute:     matchup,
		},
		"population": {
			Description: "View the current world population",
			Usage:       "[World]",
			Example:     "\"Whitesidge Ridge\"",
			Execute:     population,
		},
	},
}
