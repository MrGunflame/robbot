package template

import (
	"errors"
	"time"

	"github.com/bwmarrin/discordgo"
)

const (
	ThreadTypeCooldown uint = iota
	ThreadTypeDate
)

// Error constants
var (
	ErrInvalidCommandUsage = errors.New("Invalid command usage")
	ErrNoPermission        = errors.New("No Permission")
)

// ModuleResponse contains the data returned by the called commands goroutine
type ModuleResponse struct {
	Code  uint // 0 = noerror / 1 = invalidCommandUsage / 2 = permissionDenied / 3 = InternalServerError
	Error error
}

// A Module represents a set of commands and functionalities
// that have similar usage or work together
// modules are called using <server prefix> <module name>
// Name: the name of the module, used for help messages and to call commands of this module
// Description: basic description of the funtionanlity of this module
// Args: whether the module takes args --> no more effect
// GuildOnly: whether the module may only be called from discord servers (officially guilds)
// Commands: a list of command routes that will be claimed for this module --> no more effect
// SubCommands: a list of commands of this module, the map identifier is the command's name
// Threads: a list of background threads to be executed for each guild in specific time intervals
// Execute: not used anymore, see SubCommad.Execute and use command specific execute calls
type Module struct {
	Name        string
	Description string
	GuildOnly   bool
	Commands    map[string]Command
	Tasks       []*Task
	Permissions []string

	SQLTables []*SQLTable // A number of tables that can be used to hold persistent data
	Depends   []*Module   // Modules the module depends on
}

// SQLTable can be used to save persistent data
type SQLTable struct {
	ID      string // Unique Identifier of the table
	Columns []*SQLColumn
}

type SQLColumn struct {
	Name  string
	Type  string
	Flags []string
}

// All possiable tags for columns
const (
	FlagAutoIncrement = "AUTO_INCREMENT"
	FlagPrimaryKey    = "PRIMARY KEY"
	FlagUnsigned      = "UNSIGNED"
	FlagNotNull       = "NOT NULL"
	FlagUnique        = "UNIQUE"
)

// SubCommand represents a command owned by a module and
// can be called from it
// commands are called using <server prefix> <module name> <command name>
// Name: the name of the command, the same as the map identifier
// Description: basic description of the command, used to provide help messages
// Usage: usage of the command, used to provide help messages, the prefix and module name will be added to the start automatically
// Example: example usage of the command, used to provide help messages, prefix and module name added to the start automatically
// PermissionLevel: the required level or permission a user needs to call this command
//    0: Owner permission, only the server owner may call this command
//    1: Administator permission, only users with the administator role can call this command (defined for each server in config/servers.json)
//    2: Moderator permission: users with moderator role (defined in config/servers.json)
//    10: public, any user can call this command
// Execute: the function that will be executed on calling this command
//    takes the discord session, the message data, the args and a channel to write responses in (response codes explained in modules/router.go)

type Command struct {
	Name            string
	Description     string
	Usage           string
	Example         string
	Permissions     []string      // A list of permissions required to run this
	PermissionLevel uint          // DEPRECIATED
	ResponseTTL     time.Duration // ResponseTTL defines the time the command response can be cached and reused
	Execute         func(*discordgo.Session, *discordgo.MessageCreate, []string) error
}

// Thread represents a command or background function to be called in timed intervals
// Threads will be spawned for each server on starting the bot
// Name: the name of the thread, used for logging
// Execute: the function to be executed
// Cooldown: the time the thread will wait before restarting

// type Task struct {
// 	Name     string
// 	Execute  func(*discordgo.Session, string, chan error)
// 	Type     uint
// 	Cooldown time.Duration
// 	Date     time.Time
// }

// A Task defines a reapeating background job
type Task struct {
	Name     string
	Execute  func(s *discordgo.Session) error
	Schedule *TaskSchedule
}

// TaskSchedule defines when a task should be run
type TaskSchedule struct {
	Every    uint64
	Interval time.Duration
	Weekday  time.Weekday
	AtTime   string
}
