package permissions

import (
	"strings"

	"gitlab.com/MrGunflame/robbot/pkg/util"
	"gitlab.com/MrGunflame/robbot/store"
)

type permission struct {
	id      string
	guildID string // Only for member permissions
	node    string
}

func newPermission(id, gid, node string) *permission {
	return &permission{
		id:      id,
		guildID: gid,
		node:    node,
	}
}

func (p *permission) insert() (int, error) {
	n, err := store.Insert("INSERT INTO permissions(id, gid, node) VALUES (?, ?, ?)", p.id, p.guildID, p.node)
	if err != nil && store.ErrorCode(err) != store.SQLErrDuplicateEntry {
		return 0, err
	}
	return n, nil
}

func deletePermission(id, gid, node string) (int, error) {
	return store.Delete("DELETE FROM permissions WHERE id = ? AND gid = ? AND node = ?", id, gid, node)
}

func getPermissions(id, gid string) ([]string, error) {
	rows, err := store.Select("SELECT node FROM permissions WHERE id = ? AND gid = ?", id, gid)
	if err != nil {
		return nil, err
	}

	var nodes []string
	for rows.Next() {
		var node string
		if err = rows.Scan(&node); err != nil {
			return nil, err
		}
		nodes = append(nodes, node)
	}

	return nodes, nil
}

// userPermissions returns effective permissions of a user
func userPermissions(id, gid string, roles []string) ([]string, error) {
	// Build the query and the args
	params := []interface{}{gid, id}
	query := "SELECT node FROM permissions WHERE gid = ? AND id = ?"
	if len(roles) > 0 {
		query = util.ConcatStrings(query, " OR gid = '' AND id IN(?", strings.Repeat(",?", len(roles)-1), ")")
		for _, r := range roles {
			params = append(params, r)
		}
	}

	rows, err := store.Select(query, params...)
	if err != nil {
		return nil, err
	}

	var nodes []string
	for rows.Next() {
		var node string
		if err = rows.Scan(&node); err != nil {
			return nil, err
		}
		nodes = append(nodes, node)
	}

	return nodes, nil
}
