package permissions

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

// getRoleOrUser extracts either a role or a user (and the guildid) from the first argument
func getRoleOrUser(gID string, args *bot.Arguments) (string, string, bool) {
	arg := args.PopFirst()
	id, ok := arg.RoleID()
	if !ok {
		id, ok = arg.UserID()
		if !ok {
			return "", "", false
		}

		return id, gID, true
	}

	return id, "", true
}

func successMessage(s *discord.Session, chID, id, n, op string, role bool) {
	var mention string
	if role {
		mention = util.MentionRole(id)
	} else {
		mention = util.MentionUser(id)
	}

	s.Success(chID, "Successfully ", op, " permission node `", n, "` for ", mention, id, ">")
}
