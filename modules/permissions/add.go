package permissions

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
)

func add(ctx *bot.Context) error {
	id, gid, ok := getRoleOrUser(ctx.Event.GuildID, ctx.Arguments)
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	// Insert the permissions
	for ctx.Arguments.Next() {
		n := ctx.Arguments.PopFirst().String()
		if !validNode(n) {
			ctx.Session.Respond(ctx.Event.ChannelID, ctx.Event.Author.ID, fmt.Sprintf(":x: `%s` is no valid permission node", n))
			continue
		}
		if _, err := newPermission(id, gid, n).insert(); err != nil {
			return err
		}
		successMessage(ctx.Session, ctx.Event.ChannelID, id, n, "added", gid == "")
	}

	return nil
}
