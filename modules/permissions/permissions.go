package permissions

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/store"
)

const (
	permissionList   = "list"
	permissionManage = "manage"
)

// Module Permission is used to restrict access to the bot
// With this module you can manage permission of discord roles and user accounts
var Module = &bot.Module{
	Name:        "permissions",
	Description: "Manage command permissions and set bot permissions.",
	GuildOnly:   true,
	Commands: map[string]*bot.Command{
		"list": {
			Name:        "list",
			Description: "List a users permissions",
			Usage:       "<User>",
			Example:     "@Robbbbbbb",
			Execute:     list,
			Permissions: []string{permissionList},
		},
		"add": {
			Name:        "add",
			Description: "Add a permission to a role or user.",
			Usage:       "<Role/User> <Permission Node...>",
			Example:     "@Robbbbbbb permissions.manage",
			Execute:     add,
			Permissions: []string{permissionManage},
		},
		"remove": {
			Name:        "remove",
			Description: "Remove a permission from a role or user.",
			Usage:       "<Role/User> <Permission Node...>",
			Example:     "@Robbbbbbb permissions.manage",
			Execute:     remove,
			Permissions: []string{permissionManage},
		},
		"nodes": {
			Name:        "nodes",
			Description: "Show all avaliable nodes.",
			Usage:       "",
			Example:     "",
			Execute:     nodes,
			Permissions: []string{permissionList},
		},
		"effective": {
			Name:        "effective",
			Description: "List effective permissions of a user",
			Usage:       "<User>",
			Example:     "@Robbbbbbb",
			Execute:     effective,
			Permissions: []string{permissionList},
		},
	},
	Permissions: []string{permissionList, permissionManage},
	Store: &store.Store{
		Tables: []*store.Table{
			{
				Name: "permissions",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name: "gid",
						Type: "VARCHAR",
						Len:  32,
					},
					{
						Name:  "node",
						Type:  "VARCHAR",
						Len:   64,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
		},
	},
}
