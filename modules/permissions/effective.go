package permissions

import (
	"strings"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func effective(ctx *bot.Context) error {
	id, ok := ctx.Arguments.PopFirst().UserID()
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	user, err := ctx.Session.Bare().GuildMember(ctx.Event.GuildID, id)
	if err != nil {
		return err
	}

	nodes, err := userPermissions(id, ctx.Event.GuildID, user.Roles)
	if err != nil {
		return err
	}

	ctx.Session.ChannelMessageSendEmbed(ctx.Event.ChannelID, &discord.Embed{
		Title:       util.ConcatStrings("Effective Permissions for ", id),
		Description: strings.Join(nodes, "\n"),
	})
	return nil
}
