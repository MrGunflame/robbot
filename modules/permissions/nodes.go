package permissions

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func nodes(ctx *bot.Context) error {
	ctx.Typing()

	util.NewBigEmbed("__Permission Nodes__", util.ColorRed, AllPermissionsLen, func(i int) string {
		return util.ConcatStrings(AllPermissions[i], "\n")
	}, (64+2)).Send(ctx.Session.Bare(), ctx.Event.ChannelID)

	return nil
}
