package permissions

import (
	"gitlab.com/MrGunflame/robbot/bot"
)

func remove(ctx *bot.Context) error {
	id, gid, ok := getRoleOrUser(ctx.Event.GuildID, ctx.Arguments)
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	for ctx.Arguments.Next() {
		n := ctx.Arguments.PopFirst().String()
		if _, err := deletePermission(id, gid, n); err != nil {
			return err
		}
		successMessage(ctx.Session, ctx.Event.ChannelID, id, n, "removed", gid == "")
	}

	return nil
}
