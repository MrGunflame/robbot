package permissions

import (
	"strings"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

func list(ctx *bot.Context) error {
	id, gid, ok := getRoleOrUser(ctx.Event.GuildID, ctx.Arguments)
	if !ok {
		return ctx.InvalidCommandUsage()
	}

	nodes, err := getPermissions(id, gid)
	if err != nil {
		return err
	}

	ctx.Session.ChannelMessageSendEmbed(ctx.Event.ChannelID, &discord.Embed{
		Title:       util.ConcatStrings("Permissions for ", id),
		Description: strings.Join(nodes, "\n"),
	})

	return nil
}
