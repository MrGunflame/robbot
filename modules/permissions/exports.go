package permissions

import (
	"sort"
)

// BotOwner is the userID of the bot owner who has all permissions in any guilds
var BotOwner string

// AllPermissions contains all registered permissions
var (
	AllPermissions    sort.StringSlice
	AllPermissionsLen int
)

// validNode verifies if the node is a registered permission node
func validNode(node string) bool {
	for _, s := range AllPermissions {
		if s == node {
			return true
		}
	}
	return false
	// return sort.SearchStrings(AllPermissions, node) >= AllPermissionsLen
}

// HasPermission reports whether a user should have access
// guildOwner is the owner of the guild where the command was called
// uID is the id of the user calling the command
// roles is a slice of role ids that the user has
// nodes is a slice of the required permission nodes
func HasPermission(guildOwner, uID, gID string, roles, nodes []string) (bool, error) {
	if uID == BotOwner || uID == guildOwner {
		return true, nil
	}

	perms, err := userPermissions(uID, gID, roles)
	if err != nil {
		return false, err
	}

loop:
	for _, n := range nodes {
		for _, p := range perms {
			if n == p {
				continue loop
			}
		}
		return false, nil
	}

	return true, nil
}
