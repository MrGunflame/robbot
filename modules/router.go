package modules

import (
	"fmt"
	"log"
	"runtime/debug"
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"

	"github.com/bwmarrin/discordgo"
)

// channelCache caches channel types
// Channel ids are always unique but only keep them in memory for 1 hour
var channelCache = util.NewCache(3600 * time.Second)

func routeCommand(s *discord.Session, m *discord.MessageCreate, cmd string) error {
	// Recover a panic if a module creates one and print stacktrace
	defer func() {
		if r := recover(); r != nil {
			log.Output(4, fmt.Sprintf("[FATAL] Command '%s' caused a panic: '%s'", cmd, r))
			log.Println(string(debug.Stack()))
			s.ChannelMessageSend(m.ChannelID, ":warning: Server error occurred while running your command.")
		}
	}()

	// Parse the string into arguments
	args := bot.ParseArguments(cmd)

	if args.Len() <= 0 {
		return nil
	}

	// Get the first argument
	a1 := args.PopFirst()

	if fn, ok := coreCommands[a1.String()]; ok {
		fn(s, m, args)
		return nil
	}

	if mod, ok := modules[a1.String()]; ok {
		// Verify if guildonly command was called in guild channel
		if mod.GuildOnly {
			// Try to get channel type from cache
			val, ok := channelCache.Get(m.ChannelID)
			if !ok {
				ch, err := s.Bare().Channel(m.ChannelID)
				if err != nil {
					return err
				}

				// Insert into cache
				val = ch.Type
				channelCache.Insert(m.ChannelID, val)
			}

			if val.(discordgo.ChannelType) != discordgo.ChannelTypeGuildText {
				s.Failure(m.ChannelID, "This command cannot be used in DM channels.")
				return nil
			}
		}

		// Send a module help message when no command was given
		if !args.Next() {
			s.ChannelMessageSendEmbed(m.ChannelID, moduleHelp(mod))
			return nil
		}

		// Pass to the command
		a2 := args.PopFirst().String()
		if cmd, ok := mod.Commands[a2]; ok {
			// Permission checks
			// Skipped if the module is global (not fguildonly)
			if mod.GuildOnly {
				if ok, err := hasPermission(s, m, mod.Name, cmd.Permissions); err != nil {
					return err
				} else if !ok {
					s.Respond(m.ChannelID, m.Author.ID, ":no_entry_sign: You don't have permission to run this command.")
					return nil
				}
			}

			m2 := discord.Message(*m.Message)

			// Execute the command and handle errors
			err := cmd.Execute(&bot.Context{
				Session:   s,
				Event:     &m2,
				Arguments: args,
			})
			switch err {
			case nil:
				return nil
			case discord.ErrInvalidCommandUsage:
				s.ChannelMessageSendEmbed(m.ChannelID, commandHelp(a2, cmd))
				return nil
			default:
				return err
			}
		}
	}

	return nil
}

// RunCommand sends a created message to a command router and waits for the response
func RunCommand(s *discordgo.Session, m *discordgo.MessageCreate, cmd string) {
	m2 := discord.MessageCreate(*m)
	if err := routeCommand(discord.NewSession(s), &m2, cmd); err != nil {
		s.ChannelMessageSend(m.ChannelID, ":warning: Error occurred while running your command.")
		log.Output(3, fmt.Sprintf("[ERROR] Command failed: '%s'", err))
	}
}
