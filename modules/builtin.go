package modules

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
)

// coreCommands contains all commands that are not bound to any module
var coreCommands = map[string]func(*discord.Session, *discord.MessageCreate, *bot.Arguments) error{
	// "help":    help,
	"uptime":  uptime,
	"version": version,
	"prefix":  prefix,
}

func init() {
	coreCommands["help"] = help
}
