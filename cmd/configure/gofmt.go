package main

import (
	"log"
	"os/exec"
)

// Format a file using gofmt
func gofmt(path string) {
	if _, err := exec.LookPath("gofmt"); err != nil {
		log.Println("[INFO] Gofmt binary not found in $PATH . Skipping formatting")
		return
	}

	cmd := exec.Command("gofmt", "-s", "-w", path)
	if err := cmd.Run(); err != nil {
		log.Printf("[WARN] Failed to format file '%s' with Gofmt: '%s'", path, err)
	}
}
