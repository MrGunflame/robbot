package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"text/scanner"
)

const (
	loaderPath  = "./modules/loader.go"
	projectRoot = "gitlab.com/MrGunflame/robbot"
)

var debugMode = false

func displayError(text string, err error) {
	log.Fatalf("%s: %s", text, err)
	if debugMode {
		panic(err)
	}
}

func main() {
	log.SetFlags(0)

	if len(os.Args) < 2 {
		help()
		return
	}

	switch os.Args[1] {
	case "list":
		list()
	case "enable", "add":
		if len(os.Args[2:]) < 1 {
			log.Fatalln("No module given")
		}
		enable(os.Args[2])
	case "disable", "remove":
		if len(os.Args[2:]) < 1 {
			log.Fatalln("No module given")
		}
		disable(os.Args[2])
	default:
		help()
	}
}

// List all avaliable packages
func list() []string {
	// Open the loader file
	file, err := os.Open(loaderPath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Initialize the scanner
	var scan scanner.Scanner
	scan.Init(file)

	// LEGACY
	var packages []string

	var buf bytes.Buffer
	var open bool
	for {
		// Read a single char
		// If the file ends or the import reaches end, stop
		r := scan.Next()
		if r == scanner.EOF || r == ')' {
			break
		}

		// If we read a '"', a import path starts or ends
		if r == '"' {
			// When an import is open, we append a newline
			if open {
				if strings.HasSuffix(buf.String(), projectRoot+"/bot") {
					buf.Truncate(buf.Len() - len(projectRoot+"/bot"))
				} else {
					buf.WriteRune('\n')
				}
			}

			// LEGACY
			if !open {
				packages = append(packages, "")
			}

			// Switch open state
			open = !open
			continue
		}

		// Char is inside an import statement, so append it
		if open {
			buf.WriteRune(r)

			// LEGACY
			packages[len(packages)-1] = packages[len(packages)-1] + string(r)
		}
	}

	log.Printf("Enabled Modules: \n%s", buf.String())

	return packages
}

func disable(name string) {
	pkgs := list()

	func() {
		// Exact match
		for _, p := range pkgs {
			if p == name {
				log.Printf("[INFO] Removing module: %s", name)
				return
			}
		}

		// Module name match
		for _, p := range pkgs {
			if strings.HasSuffix(p, name) {
				log.Printf("[INFO] Removing module: %s with short path %s ", p, name)
				name = p
				return
			}
		}
	}()

	file, err := os.OpenFile(loaderPath, os.O_CREATE+os.O_RDWR, 644)
	if err != nil {
		displayError("Failed to open loader file", err)
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		displayError("Failed to read file", err)
	}

	short := func() string {
		s := strings.Split(name, "/")
		if len(s) > 0 {
			return s[len(s)-1]
		}
		return name
	}()

	str := string(content)

	// Remove from import
	if strings.Contains(str, "\t\""+name+"\"\n") {
		str = strings.Replace(str, "\t\""+name+"\"\n", "", 1)
	}

	// Remove the map entry
	str = regexp.MustCompile(`\t\"`+short+`\":[ \t]*`+short+`\.Module,\n`).ReplaceAllString(str, "")

	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	_, err = file.WriteAt([]byte(str), 0)
	if err != nil {
		panic(err)
	}

	// Format the file using gofmt
	gofmt(loaderPath)
}

func enable(name string) {
	// Remove the scheme
	name = strings.NewReplacer("https://", "", "http://", "").Replace(name)

	file, err := os.OpenFile(loaderPath, os.O_CREATE+os.O_RDWR, 644)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	str := string(content)

	short := func() string {
		s := strings.Split(name, "/")
		if len(s) > 0 {
			return s[len(s)-1]
		}
		return name
	}()

	var buf strings.Builder
	for _, r := range str {
		if r == ')' {
			buf.WriteString("\t\"")
			buf.WriteString(name)
			buf.WriteString("\"\n")
		}

		if r == '}' {
			buf.WriteString("\t\"")
			buf.WriteString(short)
			buf.WriteString("\":\t")
			buf.WriteString(short)
			buf.WriteString(".Module,\n")
		}

		buf.WriteRune(r)
	}

	_, err = file.WriteAt([]byte(buf.String()), 0)
	if err != nil {
		panic(err)
	}

	// Format the file using gofmt
	gofmt(loaderPath)
}

func help() {
	log.Println("USAGE:\n\tconfigure [OPERATION] (PATH)")

	log.Println("OPERATION:")
	for _, a := range []struct{ name, description string }{
		{
			name:        "add",
			description: "Add and Enable a new module",
		},
		{
			name:        "remove",
			description: "Disable and remove a module",
		},
		{
			name:        "list",
			description: "List all enabled modules",
		},
	} {
		log.Printf("\t%s\t%s", a.name, a.description)
	}

	log.Println("PATH:\n\tPath to the module")
}
