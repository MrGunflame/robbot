package bot

import (
	"time"

	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/store"
)

// A Module is a namespace for creating custom commands and bot functionalities
type Module struct {
	Name        string
	Description string
	GuildOnly   bool
	Commands    map[string]*Command
	Permissions []string
	Tasks       []*Task
	Store       *store.Store
}

// A Command is a command of a module that responds to a user calling it
type Command struct {
	Name        string // DEPRECEATED, use the map key as name instead
	Description string
	Usage       string
	Example     string
	Permissions []string
	Execute     func(*Context) error
}

// Task is a background job that should be executed automatically without any user interaction
type Task struct {
	Name      string
	Interval  time.Duration
	AtTime    string                               // In the format: HH:MM
	AtWeekday time.Weekday                         // Optional weekday for At field
	Execute   func(*discord.Session, string) error // Include the session and the discord guild
}
