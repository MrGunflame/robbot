package bot

import (
	"bytes"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/MrGunflame/robbot/pkg/util"
)

var (
	regexUserMention    = regexp.MustCompile(`<@!?\d+>`)
	regexRoleMention    = regexp.MustCompile(`<@&\d+>`)
	regexChannelMention = regexp.MustCompile(`<#\d+>`)
)

// type ArgumentType uint8

// const (
// 	ArgumentTypeString ArgumentType = iota
// 	ArgumentTypeInt
// 	ArgumentUserID
// 	ArgumentRoleID
// 	ArgumentChannelID
// )

// A Argument is a argument for a command parsed from a discord message
type Argument struct {
	raw string
}

// String returns the raw input argument
func (a *Argument) String() string {
	return a.raw
}

// Int returns the argument as an int
func (a *Argument) Int() (int, bool) {
	n, err := strconv.Atoi(a.raw)
	return n, err == nil
}

// UserID returns the argument as an userid
func (a *Argument) UserID() (string, bool) {
	return util.ExtractUserID(a.raw), regexUserMention.MatchString(a.raw)
}

// RoleID returns the argument as a roleid
func (a *Argument) RoleID() (string, bool) {
	return util.ExtractRoleID(a.raw), regexRoleMention.MatchString(a.raw)
}

// ChannelID returns the argument as a channelid
func (a *Argument) ChannelID() (string, bool) {
	return util.ExtractChannelID(a.raw), regexChannelMention.MatchString(a.raw)
}

// Arguments contains multiple arguments
type Arguments struct {
	args []*Argument
}

// Len returns the number of all arguments
func (a *Arguments) Len() int {
	return len(a.args)
}

// Get returns a single argument
func (a *Arguments) Get(i int) *Argument {
	if i >= a.Len() {
		return nil
	}
	return a.args[i]
}

// PopFirst splits off the first argument from the argument list and returns it
func (a *Arguments) PopFirst() *Argument {
	arg := a.Get(0)
	if arg == nil {
		return &Argument{}
	}

	a.args = a.args[1:]
	return arg
}

// Next returns true while arguments still contains arguments
func (a *Arguments) Next() bool {
	return a.Len() > 0
}

// First returns the first argument
// Equavalent of a.Get(0)
func (a *Arguments) First() *Argument {
	return a.Get(0)
}

// Last returns the last argument
// Equivalent of a.Get(a.Len()-1)
func (a *Arguments) Last() *Argument {
	return a.Get(a.Len() - 1)
}

// String returns all arguments as a single string
func (a *Arguments) String() string {
	switch a.Len() {
	case 0:
		return ""
	case 1:
		return a.args[0].String()
	}

	var b strings.Builder
	b.WriteString(a.args[0].String())
	for _, arg := range a.args[1:] {
		b.WriteString(" ")
		b.WriteString(arg.String())
	}

	return b.String()
}

// ParseArguments splits a string into multiple arguments
func ParseArguments(s string) *Arguments {
	var args []*Argument

	// Converting string to []byte for better performance
	raw := []byte(s)

	buf := bytes.NewBuffer([]byte{})
	// Grow the buffer to the maximum possible length
	buf.Grow(len(raw))
	esc := false
	for _, b := range raw {
		if b == ' ' && !esc {
			args = append(args, &Argument{
				raw: buf.String(),
			})
			buf.Reset()
			continue
		}

		if b == '"' {
			esc = !esc
			continue
		}

		buf.WriteByte(b)
	}

	// Write the rest of the buffer
	if buf.Len() > 0 {
		args = append(args, &Argument{
			raw: buf.String(),
		})
	}

	return &Arguments{
		args: args,
	}
}

type TokenType uint8

const (
	TokenTypeUnknown TokenType = iota
	TokenTypeString
	TokenTypeInt
)

type Token struct {
	Type     TokenType
	Name     string
	Optional bool
}

func Extract(args *Arguments, usage string, loc *map[string]interface{}) {

	buf := bytes.NewBuffer([]byte{})

	usage = "<int Event-ID> <string null> [int hh[:int mm[:int ss]]]"

	depth := 0
	var tokens []*Token
	// typeDef := false
	for _, c := range usage {
		switch c {
		case '<':
			depth++
			buf.Reset()
			tokens = append(tokens, &Token{
				Type:     TokenTypeUnknown,
				Name:     "",
				Optional: false,
			})
		case '>':
			depth--
			tokens[depth].Name = buf.String()
			buf.Reset()
		case '[':
			depth++
			buf.Reset()
		case ']':
			depth--
			buf.Reset()
		case ' ':
			t := tokens[depth-1]
			switch buf.String() {
			case "int":
				t.Type = TokenTypeInt
			case "string":
				t.Type = TokenTypeString
			default:
				t.Type = TokenTypeUnknown
			}

			buf.Reset()
		default:
			buf.WriteRune(c)
		}
	}

}
