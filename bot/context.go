package bot

import (
	"log"

	"gitlab.com/MrGunflame/robbot/discord"

	"github.com/bwmarrin/discordgo"
)

// Context contains the context for a function call
type Context struct {
	Session   *discord.Session
	Event     *discord.Message
	Arguments *Arguments
}

// InvalidCommandUsage returns an error that registers the command call to be invalid and an command help will be shown instead
// The return value from this function should be used as a command handler return value
// E.g.:
// if (condition) {
//	return ctx.InvalidCommandUsage()
// }
func (c *Context) InvalidCommandUsage() error {
	return c.Session.InvalidCommandUsage()
}

// Success sends a success message with the given string
func (c *Context) Success(msg string) {
	c.Session.Success(c.Event.ChannelID, msg)
}

// Failure sends a failure message with the given string
func (c *Context) Failure(msg string) {
	c.Session.Failure(c.Event.ChannelID, msg)
}

// Typing broadcasts the bot as typing
func (c *Context) Typing() {
	c.Session.ChannelTyping(c.Event.ChannelID)
}

// ReplyEmbed sends a embed in the same channel a message was received
// DEPRECEATED. use c.RespondEmbed instead
func (c *Context) ReplyEmbed(embed *discord.Embed) error {
	return c.RespondEmbed(embed)
}

// RespondEmbed sends a embed in the same channel a message was received
func (c *Context) RespondEmbed(embed *discord.Embed) error {
	_, err := c.Session.ChannelMessageSendEmbed(c.Event.ChannelID, embed)
	return err
}

// Respond responds to a event
func (c *Context) Respond(msg string) (*discordgo.Message, error) {
	return c.Session.Bare().ChannelMessageSend(c.Event.ChannelID, msg)
}

// RunTask manually runs executes a task for a single guild
func (c *Context) RunTask(fn func(*discord.Session, string) error) {
	c.Respond(":information_source:  Starting new task")
	if err := fn(c.Session, c.Event.GuildID); err != nil {
		c.Failure("Task failed")
		log.Printf("[ERROR] Manually spawned task from guild '%s' failed: %s", c.Event.GuildID, err)
		return
	}
	c.Success("Task complete")
}

// DeleteMessage deletes the message that triggered the command
func (c *Context) DeleteMessage() error {
	return c.Session.ChannelMessageDelete(c.Event.ChannelID, c.Event.ID)
}
