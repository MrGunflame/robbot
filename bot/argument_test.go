package bot

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestArgumentsString(t *testing.T) {
	cases := []struct {
		given  *Arguments
		expect string
	}{
		{
			given: &Arguments{
				args: []*Argument{},
			},
			expect: "",
		},
		{
			given: &Arguments{
				args: []*Argument{
					{
						raw: "Hello",
					},
				},
			},
			expect: "Hello",
		},
		{
			given: &Arguments{
				args: []*Argument{
					{
						raw: "Hello",
					},
					{
						raw: "World",
					},
				},
			},
			expect: "Hello World",
		},
	}

	for _, c := range cases {
		result := c.given.String()
		if result != c.expect {
			t.Errorf("String failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestParseArguments(t *testing.T) {
	cases := []struct {
		given  string
		expect *Arguments
	}{
		{
			given: "test",
			expect: &Arguments{
				args: []*Argument{
					{
						raw: "test",
					},
				},
			},
		},
		{
			given: "two arguments",
			expect: &Arguments{
				args: []*Argument{
					{
						raw: "two",
					},
					{
						raw: "arguments",
					},
				},
			},
		},
		{
			given: "\"escaped argument\"",
			expect: &Arguments{
				args: []*Argument{
					{
						raw: "escaped argument",
					},
				},
			},
		},
		{
			given: "first argument \"escaped argument\" last argument",
			expect: &Arguments{
				args: []*Argument{
					{
						raw: "first",
					},
					{
						raw: "argument",
					},
					{
						raw: "escaped argument",
					},
					{
						raw: "last",
					},
					{
						raw: "argument",
					},
				},
			},
		},
	}

	for _, c := range cases {
		result := ParseArguments(c.given)
		if !cmp.Equal(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})) {
			t.Errorf("ParseArguments failed: '%s'", cmp.Diff(result, c.expect, cmp.AllowUnexported(Arguments{}, Argument{})))
		}
	}
}

func BenchmarkParseArguments(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseArguments("Hello World \"hello world\"")
	}
}
