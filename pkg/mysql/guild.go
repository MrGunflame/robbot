package mysql

import (
	"database/sql"
	"errors"

	"gitlab.com/MrGunflame/robbot/pkg/config"
)

type GuildMember struct {
	GuildWars2 string
	Discord    string
}

var (
	MemberAlreadyExistsErr = errors.New("Member already exists")
)

func AddGuildMember(guildwars2, discord string) (err error) {

	if duplicate, err := checkDuplicate(guildwars2); err != nil {
		return err
	} else if duplicate {
		return MemberAlreadyExistsErr
	}

	stmt, err := config.DBCon.Prepare("INSERT INTO guildmembers (guildwars2, discord) VALUES (?, ?)")
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(guildwars2, discord)
	if err != nil {
		return
	}

	return
}

func UpdateGuildMember(guildwars2, discord string) (err error) {
	return
}

// Remove a Guild Member from Datbase
// t defines how to select the member: 1 is by accountname, 2 is by discord
func RemoveGuildMember(t uint, name string) (err error) {

	var (
		stmt *sql.Stmt
	)

	if t == 1 {
		stmt, err = config.DBCon.Prepare("DELETE FROM guildmembers WHERE guildwars2 = ?")
		if err != nil {
			return err
		}
	} else if t == 2 {
		stmt, err = config.DBCon.Prepare("DELETE FROM guildmembers WHERE discord = ?")
		if err != nil {
			return err
		}
	}
	defer stmt.Close()

	_, err = stmt.Exec(name)
	if err != nil {
		return
	}

	return
}

func checkDuplicate(guildwars2 string) (duplicate bool, err error) {

	stmt, err := config.DBCon.Prepare("SELECT '' FROM guildmembers WHERE guildwars2 = ?")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(guildwars2)
	if err != nil {
		return
	}

	for rows.Next() {
		duplicate = true
	}
	return
}

func GetGuildMember(discord string) (member GuildMember, err error) {

	stmt, err := config.DBCon.Prepare("SELECT guildwars2, discord FROM guildmembers WHERE discord = ?")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(discord)
	if err != nil {
		return
	}

	for rows.Next() {
		err = rows.Scan(&member.GuildWars2, &member.Discord)
		if err != nil {
			return
		}
	}

	return
}

// GetGuildMembers will return all guild members
func GetGuildMembers() (members []GuildMember, err error) {

	stmt, err := config.DBCon.Prepare("SELECT guildwars2, discord FROM guildmembers")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return
	}

	var member GuildMember
	for rows.Next() {
		err = rows.Scan(&member.GuildWars2, &member.Discord)
		if err != nil {
			return
		}
		members = append(members, member)
	}

	return
}
