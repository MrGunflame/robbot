package mysql

import (
	"gitlab.com/MrGunflame/robbot/pkg/config"
)

// funcs for table apikeys
// storing apikeys of users

// AddAPIKey will add a new apikey linked to a user by discord id
func AddAPIKey(discord, key string) (err error) {

	stmt, err := config.DBCon.Prepare("INSERT INTO apikeys (discord, apikey) VALUES (?, ?)")
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(discord, key)
	if err != nil {
		return
	}
	return
}

func GetAPIKeys(discord string) (keys []string, err error) {

	stmt, err := config.DBCon.Prepare("SELECT apikey FROM apikeys WHERE discord = ?")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(discord)
	if err != nil {
		return
	}

	var key string
	for rows.Next() {
		err = rows.Scan(&key)
		if err != nil {
			return
		}
		keys = append(keys, key)
	}

	return
}

func GetAllAPIKeys() (keys []string, err error) {

	stmt, err := config.DBCon.Prepare("SELECT apikey FROM apikeys")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return
	}

	var key string
	for rows.Next() {
		err = rows.Scan(&key)
		if err != nil {
			return
		}
		keys = append(keys, key)
	}

	return
}

func RemoveAPIKey(key string) (err error) {

	stmt, err := config.DBCon.Prepare("DELETE FROM apikeys WHERE apikey = ?")
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(key)
	if err != nil {
		return
	}

	return
}
