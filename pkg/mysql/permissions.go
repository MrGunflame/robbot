package mysql

import (
	"errors"
	"strings"

	"gitlab.com/MrGunflame/robbot/pkg/config"
	"gitlab.com/MrGunflame/robbot/pkg/util"
)

const (
	PermissionTypeMember uint = iota
	PermissionTypeRole
)

var (
	PermissionExistsErr = errors.New("Permission Node already exists for id")
)

// HasPermissionRole returns whether all the input roles together have a permission node
func HasPermissionRole(roles []string, node string) (permission bool, err error) {

	stmt, err := config.DBCon.Prepare(util.ConcatStrings("SELECT COUNT(*) FROM permissions WHERE discord IN(?", strings.Repeat(",?", len(roles)-1), ") AND node = ?"))
	if err != nil {
		return
	}
	defer stmt.Close()

	var numRows int

	var args []interface{}
	for _, role := range roles {
		args = append(args, role)
	}
	args = append(args, node)

	if err = stmt.QueryRow(args...).Scan(&numRows); err != nil {
		return
	}

	return numRows >= 1, err
}

func PermissionsExist(permissionType uint, discord, node string) (exists bool, err error) {

	stmt, err := config.DBCon.Prepare("SELECT COUNT(*) FROM permissions WHERE type = ? AND discord = ? AND node = ?")
	if err != nil {
		return
	}
	defer stmt.Close()

	var numRows int

	if err = stmt.QueryRow(permissionType, discord, node).Scan(&numRows); err != nil {
		return
	}

	return numRows >= 1, err
}

func GetPermissions(permissionType uint, discord string) (nodes []string, err error) {

	stmt, err := config.DBCon.Prepare("SELECT node FROM permissions WHERE type = ? AND discord = ? ORDER BY node")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(permissionType, discord)
	if err != nil {
		return
	}

	var node string
	for rows.Next() {
		err = rows.Scan(&node)
		if err != nil {
			return
		}
		nodes = append(nodes, node)
	}

	return
}

func AddPermission(permissionType uint, discord, node string) (err error) {

	if exists, err := PermissionsExist(permissionType, discord, node); err != nil {
		return err
	} else if exists {
		return PermissionExistsErr
	}

	stmt, err := config.DBCon.Prepare("INSERT INTO permissions(type, discord, node) VALUES (?, ?, ?)")
	if err != nil {
		return
	}
	defer stmt.Close()

	if _, err := stmt.Exec(permissionType, discord, node); err != nil {
		return err
	}

	return
}

func RemovePermission(permissionType uint, discord, node string) (err error) {

	stmt, err := config.DBCon.Prepare("DELETE FROM permissions WHERE type = ? AND discord = ? AND node = ?")
	if err != nil {
		return
	}
	defer stmt.Close()

	if _, err := stmt.Exec(permissionType, discord, node); err != nil {
		return err
	}

	return
}
