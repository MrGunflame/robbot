package mysql

import (
	"gitlab.com/MrGunflame/robbot/pkg/config"
)

type LeaderboardAccountStats struct {
	Account    string
	KillsMatch uint
}

func AddLeaderboardUser(account string, killsMatch uint) (err error) {

	stmt, err := config.DBCon.Prepare("INSERT INTO leaderboard (account, kills_match) VALUES (?, ?)")
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(account, killsMatch)
	if err != nil {
		return
	}
	return
}

// GetLeaderboardStats returns all data contained in the leaderboard table
// with account and all saved stats
func GetLeaderboardStats() (stats []LeaderboardAccountStats, err error) {

	stmt, err := config.DBCon.Prepare("SELECT account, kills_match FROM leaderboard")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return
	}

	var account LeaderboardAccountStats
	for rows.Next() {
		err = rows.Scan(&account.Account, &account.KillsMatch)
		if err != nil {
			return
		}
		stats = append(stats, account)
	}

	return
}

func ResetLeaderboardStats() (err error) {

	stmt, err := config.DBCon.Prepare("DELETE FROM leaderboard")
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return
	}

	return
}
