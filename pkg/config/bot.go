package config

// Global bot config for the discord api

import (
	"encoding/json"
	"os"
)

type Bot struct {
	Token   string
	OwnerID string
	Prefix  string
}

func LoadBotConfig(filename string) (conf Bot, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&conf)
	if err != nil {
		return
	}

	return
}
