package config

// Server-wide config for each discord server

import (
	"encoding/json"
	"os"
)

// Server contains entires for each registered server
// DEPRECEATED: Use the sql package instead and store your data in tables
var Server map[string]ServerConfig

// ServerConfig holds static data for a server required by modules
// DEPRECEATED: Use the sql package instead and store your data in tables
type ServerConfig struct {
	ID           string
	Prefix       string
	VerifiedRole string
	Ranks        map[string]string
	AdminRole    string
	Guild        struct {
		ID     string
		ApiKey string
		Role   []struct {
			Name string // GW2NAME
			ID   string // DISCORD ID
		}
	}
	// The guildsync module
	GuildSync struct {
		MemberRole string
	}
	// The wvw module
	WvW struct {
		DefaultWorld int
	}
	// The role module
	Role struct {
		Roles map[string]string
	}
}

// LoadServerConfig loads the serverconfig from a given file
// DEPRECEATED: Use the sql package instead and store your data in tables
func LoadServerConfig(filename string) (conf map[string]ServerConfig, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&conf)
	if err != nil {
		return
	}

	return
}
