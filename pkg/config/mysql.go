package config

// Global bot config for mysql connections

import (
	"database/sql"
	"encoding/json"
	"os"

	"gitlab.com/MrGunflame/robbot/pkg/util"
)

var DBCon *sql.DB

type Mysql struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func LoadMysqlConfig(filename string) (conf Mysql, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&conf)
	if err != nil {
		return conf, err
	}
	return
}

func GenerateMysqlLink(conf Mysql) string {
	return util.ConcatStrings(conf.User, ":", conf.Password, "@tcp(", conf.Host, ":", conf.Port, ")/", conf.Database)
}
