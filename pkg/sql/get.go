package sql

import (
	"database/sql"
)

// Get makes a database query and returns the result rows
// query is the sql query, args the query arguments
func Get(query string, args ...interface{}) (*sql.Rows, error) {
	stmt, err := DBConn.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	return stmt.Query(args...)
}
