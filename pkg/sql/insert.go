package sql

// Insert inserts a new row into a table
// query is the sql query, args the query arguments
// returns an error and the last inserted id
func Insert(query string, args ...interface{}) (int, error) {
	stmt, err := DBConn.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}
