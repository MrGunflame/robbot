package sql

// SetupDatabase creates all tables required
func SetupDatabase(db string) error {
	var tables = []string{
		"permissions",
		"colorsync",
		"token",
	}
	var setupQueries = []string{
		"CREATE TABLE permissions(id VARCHAR(32) NOT NULL, node VARCHAR(32) NOT NULL, type VARCHAR(16) NOT NULL)",
		"CREATE TABLE colorsync(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, channel VARCHAR(32) NOT NULL, guild VARCHAR(32) NOT NULL, color VARCHAR(32) NOT NULL)",
		"CREATE TABLE token(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, owner VARCHAR(32) NOT NULL, token VARCHAR(128) NOT NULL)",
		"CREATE TABLE guildmembers(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, guildwars2 VARCHAR(32) NOT NULL, discord VARCHAR(32) NOT NULL)",
	}

	existStmt, err := DBConn.Prepare("SELECT table_schema FROM information_schema.tables WHERE table_schema = ? AND table_name = ? LIMIT 1")
	if err != nil {
		return err
	}

	for i, t := range tables {

		// Search for existing table
		var found bool
		rows, err := existStmt.Query(db, t)
		if err != nil {
			return err
		}

		for rows.Next() {
			found = true
		}

		// Create it when not found
		if !found {
			if _, err := DBConn.Exec(setupQueries[i]); err != nil {
				return err
			}
		}
	}

	return nil
}
