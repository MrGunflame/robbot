package sql

// Delete deletes a set of rows from a table
// query is the sql query, args the query arguments
// returns an error and the number of affected rows
func Delete(query string, args ...interface{}) (int, error) {
	stmt, err := DBConn.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	num, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(num), nil
}
