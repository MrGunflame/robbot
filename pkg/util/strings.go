package util

import (
	"strings"
)

// ConcatStrings concates multiple strings
func ConcatStrings(args ...string) string {
	switch len(args) {
	case 0:
		return ""
	case 1:
		return args[0]
	}

	var b strings.Builder

	var n int
	for _, s := range args {
		n += len(s)
	}
	b.Grow(n)

	for _, s := range args {
		b.WriteString(s)
	}
	return b.String()
}
