package util

import (
	"time"
)

// Cache is a memory cache that holds any type for the given duration
type Cache struct {
	lifetime time.Duration
	data     map[string]struct {
		created time.Time
		cont    interface{}
	}
}

// NewCache returns a new cache
func NewCache(lifetime time.Duration) *Cache {
	return &Cache{
		lifetime: lifetime,
		data: make(map[string]struct {
			created time.Time
			cont    interface{}
		}),
	}
}

// Insert inserts a new item into the cache
func (c *Cache) Insert(key string, val interface{}) {
	c.data[key] = struct {
		created time.Time
		cont    interface{}
	}{
		created: time.Now(),
		cont:    val,
	}
	go func() {
		<-time.After(c.lifetime)
		c.Delete(key)
	}()
}

// Get returns the item with the key from the cache
func (c *Cache) Get(key string) (interface{}, bool) {
	val, ok := c.data[key]
	if !ok || time.Now().Sub(val.created) > c.lifetime {
		return nil, false
	}
	return val.cont, true
}

// Delete removes an item from the cache
func (c *Cache) Delete(key string) {
	delete(c.data, key)
}
