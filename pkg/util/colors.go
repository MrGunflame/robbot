package util

// -----------------------------------
// ---         CSS LEVEL 1         ---
// --- https://www.w3.org/TR/CSS1/ ---
// -----------------------------------
const (
	ColorBlack   = 0x000000
	ColorSilver  = 0xc0c0c0
	ColorGray    = 0x808080
	ColorWhite   = 0xffffff
	ColorMaroon  = 0x800000
	ColorRed     = 0xff0000
	ColorPurple  = 0x800080
	ColorFuchsia = 0xff00ff
	ColorGreen   = 0x008000
	ColorLime    = 0x00ff00
	ColorOlive   = 0x808000
	ColorYellow  = 0xffff00
	ColorNavy    = 0x000080
	ColorBlue    = 0x0000ff
	ColorTeal    = 0x008080
	ColorAqua    = 0x00ffff
)

// -----------------------------------
// ---  CSS Level 2 (Revision 1)   ---
// --- https://www.w3.org/TR/CSS2/ ---
// -----------------------------------
const (
	ColorOrange = 0xffa500
)
