package util

import (
	"reflect"
	"testing"

	"github.com/bwmarrin/discordgo"
)

func TestResponseData(t *testing.T) {

	cases := []struct {
		given struct {
			userid string
			msg    string
		}
		expect *discordgo.MessageSend
	}{
		{
			given: struct {
				userid string
				msg    string
			}{
				userid: "1",
				msg:    "Hello World!",
			},
			expect: &discordgo.MessageSend{
				Content: "<@1>, Hello World!",
			},
		},
	}

	for _, c := range cases {
		result := ResponseData(c.given.userid, c.given.msg)
		if !reflect.DeepEqual(result, c.expect) {
			t.Error("ResponseData returned ", result, " but expected ", c.expect)
		}
	}

}

func TestNewEmbed(t *testing.T) {

	cases := []struct {
		expect *discordgo.MessageEmbed
	}{
		{
			expect: &discordgo.MessageEmbed{
				Color: 15746887,
			},
		},
	}

	for _, c := range cases {
		result := NewEmbed()
		if !reflect.DeepEqual(result, c.expect) {
			t.Error("NewEmbed returned ", result, " but expected ", c.expect)
		}
	}

}

func TestExtractUserID(t *testing.T) {
	cases := []struct {
		given  string
		expect string
	}{
		{
			given:  "<@1234>",
			expect: "1234",
		},
		{
			given:  "<@!5678>",
			expect: "5678",
		},
	}

	for _, c := range cases {
		result := ExtractUserID(c.given)
		if result != c.expect {
			t.Errorf("ExtractUserID failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestExtractRoleID(t *testing.T) {
	cases := []struct {
		given  string
		expect string
	}{
		{
			given:  "<@&1234>",
			expect: "1234",
		},
	}

	for _, c := range cases {
		result := ExtractRoleID(c.given)
		if result != c.expect {
			t.Errorf("ExtractRoleID failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}

func TestExtractChannelID(t *testing.T) {
	cases := []struct {
		given  string
		expect string
	}{
		{
			given:  "<#1234>",
			expect: "1234",
		},
	}

	for _, c := range cases {
		result := ExtractChannelID(c.given)
		if result != c.expect {
			t.Errorf("ExtractChannelID failed: '%s' was expected but '%s' was returned", c.expect, result)
		}
	}
}
