package util

import (
	"errors"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Error constants
var (
	ErrNotFound = errors.New("not found")
)

// ResponseData returns the data to send a @User, [msg] message
func ResponseData(userid, msg string) *discordgo.MessageSend {
	return &discordgo.MessageSend{
		Content: ConcatStrings("<@", userid, ">, ", msg),
	}
}

// NewEmbed returns a new Embed with the default color (red)
func NewEmbed() *discordgo.MessageEmbed {
	return &discordgo.MessageEmbed{
		Color: 15746887,
	}
}

// ChannelFromString extracts a discord channelid from a string
// when no channel was found it returns false
func ChannelFromString(s string) (string, bool) {
	if !strings.HasPrefix(s, "<#") || !strings.HasSuffix(s, ">") {
		return "", false
	}
	return strings.Replace(strings.Replace(s, "<#", "", 1), ">", "", 1), true
}

// MentionFromString extracts a discord userid from a string
// when no user was found it returns false
func MentionFromString(s string) (string, bool) {
	if !strings.HasPrefix(s, "<@") || !strings.HasSuffix(s, ">") {
		return "", false
	}
	return strings.Replace(strings.Replace(s, "<@", "", 1), ">", "", 1), true
}

// RoleFromString extracs a discord role mention from a strings
// when no mention was found it returns false
func RoleFromString(s string) (string, bool) {
	if !strings.HasPrefix(s, "<@&") || !strings.HasSuffix(s, ">") {
		return "", false
	}
	return strings.Replace(strings.Replace(s, "<@&", "", 1), ">", "", 1), true
}

// ChannelArgument extracts the channel from a string argument
// It returns the channel id, the channel type. The error is ErrNotFound when the channel wasn't found
// s is the discord Session, name the requested channel and gID the guild id where to search
// When multiple channels with the same name are present, the first one found will be returned
func ChannelArgument(s *discordgo.Session, name, gID string) (string, discordgo.ChannelType, error) {
	// Check if the channel has the format <#ID>, then it is a text channel
	if strings.HasPrefix(name, "<#") && strings.HasSuffix(name, ">") {
		return strings.Replace(strings.Replace(name, "<@", "", 1), ">", "", 1), discordgo.ChannelTypeGuildText, nil
	}

	// Get all channels
	chs, err := s.GuildChannels(gID)
	if err != nil {
		return "", 0, err
	}

	// Find the matching channel
	for _, c := range chs {
		if c.Name == name {
			return c.ID, c.Type, nil
		}
	}

	return "", 0, ErrNotFound
}

// CategoryFromName gets a category from it's name
func CategoryFromName(s *discordgo.Session, name, gID string) (string, error) {
	chs, err := s.GuildChannels(gID)
	if err != nil {
		return "", err
	}

	for _, c := range chs {
		if c.Type == discordgo.ChannelTypeGuildCategory && c.Name == name {
			return c.ID, nil
		}
	}

	return "", ErrNotFound
}

// ExtractUserID extracts a discord user id from a mention
func ExtractUserID(mention string) string {
	return strings.NewReplacer("<@!", "", "<@", "", ">", "").Replace(mention)
}

// ExtractRoleID extracts a discord role id from a mention
func ExtractRoleID(mention string) string {
	return strings.NewReplacer("<@&", "", ">", "").Replace(mention)
}

// ExtractChannelID extracts a discord channel id from a mention
func ExtractChannelID(mention string) string {
	return strings.NewReplacer("<#", "", ">", "").Replace(mention)
}

// MentionUser returns a mention to a user
func MentionUser(id string) string {
	return ConcatStrings("<@", id, ">")
}

// MentionRole returns a mention to a role
func MentionRole(id string) string {
	return ConcatStrings("<@&", id, ">")
}

// MentionChannel returns a mentino to a channel
func MentionChannel(id string) string {
	return ConcatStrings("<#", id, ">")
}
