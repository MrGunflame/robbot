package util

import (
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestNewCache(t *testing.T) {
	cases := []struct {
		given  time.Duration
		expect *Cache
	}{
		{
			given: 1 * time.Second,
			expect: &Cache{
				lifetime: 1 * time.Second,
				data: make(map[string]struct {
					created time.Time
					cont    interface{}
				}),
			},
		},
	}
	for _, c := range cases {
		result := NewCache(c.given)
		if !cmp.Equal(result, c.expect, cmp.AllowUnexported(Cache{})) {
			t.Errorf("NewCache failed: '%s'", cmp.Diff(result, c.expect, cmp.AllowUnexported(Cache{})))
		}
	}
}

func TestCacheInsert(t *testing.T) {
	now := time.Now()
	cases := []struct {
		given struct {
			cache *Cache
			key   string
			val   interface{}
		}
		expect *Cache
	}{
		{
			given: struct {
				cache *Cache
				key   string
				val   interface{}
			}{
				cache: &Cache{
					lifetime: 1 * time.Hour,
					data: make(map[string]struct {
						created time.Time
						cont    interface{}
					}),
				},
				key: "Test",
				val: "value",
			},
			expect: &Cache{
				lifetime: 1 * time.Hour,
				data: map[string]struct {
					created time.Time
					cont    interface{}
				}{
					"Test": struct {
						created time.Time
						cont    interface{}
					}{
						created: now,
						cont:    "value",
					},
				},
			},
		},
	}

	for _, c := range cases {
		c.given.cache.Insert(c.given.key, c.given.val)

		// Skip validation of the created field
		c.given.cache.data[c.given.key] = struct {
			created time.Time
			cont    interface{}
		}{
			created: now,
			cont:    c.given.cache.data[c.given.key].cont,
		}

		if !cmp.Equal(c.given.cache, c.expect, cmp.AllowUnexported(Cache{}, struct {
			created time.Time
			cont    interface{}
		}{})) {
			t.Errorf("Insert failed: '%s'", cmp.Diff(c.given.cache, c.expect, cmp.AllowUnexported(Cache{}, struct {
				created time.Time
				cont    interface{}
			}{})))
		}
	}
}
