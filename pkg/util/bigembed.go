package util

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

// BigEmbed contains multiple embeds with descriptions longer than the maximum length of 2048
type BigEmbed struct {
	name  string
	color int
	buf   []strings.Builder
}

// NewBigEmbed creates a new big embed
// name is the embed title
// color is the embed color
// ln is the length of the slice of items that should be displayed
// getFn should return the string to append at a specific index
// maxLen is the maximun possible length of an entry. It is used to define when to create a new embed
func NewBigEmbed(name string, color int, ln int, getFn func(int) string, maxLen int) *BigEmbed {
	buf := []strings.Builder{strings.Builder{}}
	i := 0
	for j := 0; j < ln; j++ {
		buf[i].WriteString(getFn(j))

		if buf[i].Len() > (2048-maxLen) && j+1 < ln {
			buf = append(buf, strings.Builder{})
			i++
		}
	}

	return &BigEmbed{
		name:  name,
		color: color,
		buf:   buf,
	}
}

// Send sends all embeds to the given channel
func (e *BigEmbed) Send(s *discordgo.Session, chID string) ([]*discordgo.Message, error) {
	var msgs []*discordgo.Message
	for _, b := range e.buf {
		msg, err := s.ChannelMessageSendEmbed(chID, &discordgo.MessageEmbed{
			Title:       e.name,
			Color:       e.color,
			Description: b.String(),
		})
		if err != nil {
			return nil, err
		}
		msgs = append(msgs, msg)
	}
	return msgs, nil
}
