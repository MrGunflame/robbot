package util

import (
	"testing"
)

func TestConcatStrings(t *testing.T) {
	cases := []struct {
		given  []string
		expect string
	}{
		{
			given:  []string{"String1", "String2"},
			expect: "String1String2",
		},
	}

	for _, c := range cases {
		result := ConcatStrings(c.given...)
		if result != c.expect {
			t.Errorf("ConcatStrings returned '%s' but '%s' was expected", result, c.expect)
		}
	}
}

func BenchmarkConcatStrings(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcatStrings("Test", "Test2")
	}
}
