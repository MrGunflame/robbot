package hello

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
)

// Module hello says Hello World!
var Module = &bot.Module{
	// This will be the name of your module
	Name: "hello",
	Commands: map[string]*bot.Command{
		// Register a command with the name "world" to the module
		"world": {
			Description: "Hello World!",
			Usage:       "",
			Example:     "",
			// Execute is a function with the type func(*bot.Context) error
			// It must be given for the command, otherwise the bot will not work
			Execute: hello,
		},
		"greet": {
			Description: "Hello World with Arguments!",
			// <ARG-NAME> should be used for required arguments
			// [ARG-NAME] should be used for optional arguments
			Usage:   "<Name>",
			Example: "Robb",
			Execute: greet,
		},
	},
}

func hello(ctx *bot.Context) error {
	// Respond sends a text message in the same channel as the message was received
	ctx.Respond("Hello World!")

	// Always return a nil error at the end of your command
	return nil
}

func greet(ctx *bot.Context) error {
	// Access the arguments with the Arguments field
	args := ctx.Arguments

	// Now we take the first argument and extract it as a string
	name := args.PopFirst().String()

	ctx.Respond(fmt.Sprintf("Hello %s!", name))
	return nil
}
