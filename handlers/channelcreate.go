package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// ChannelCreate triggers when a channel is created
func ChannelCreate(s *discordgo.Session, st *discordgo.ChannelCreate) {
	go modules.SendEvent(discord.EventTypeChannelCreate, st)
}
