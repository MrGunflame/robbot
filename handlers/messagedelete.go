package handlers

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

// MessageDeleteHandler is called when a message was deleted
func MessageDeleteHandler(s *discordgo.Session, m *discordgo.MessageDelete) {
	fmt.Println(m.Message)
}
