package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// GuildCreate triggers when a new guild has been joined or a unavaliable guild got loaded
func GuildCreate(s *discordgo.Session, st *discordgo.GuildCreate) {
	go modules.SendEvent(discord.EventTypeGuildCreate, st)
}
