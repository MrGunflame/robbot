package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// MessageReactionAdd triggers when a user reaced to a message
func MessageReactionAdd(s *discordgo.Session, st *discordgo.MessageReactionAdd) {
	go modules.SendEvent(discord.EventTypeMessageReactionAdd, st)
}
