package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// Ready contains the initial state information
func Ready(s *discordgo.Session, r *discordgo.Ready) {
	go modules.SpawnTasks(discord.NewSession(s))
}
