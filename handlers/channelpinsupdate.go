package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// ChannelPinsUpdate triggers when a channel pin is updated
func ChannelPinsUpdate(s *discordgo.Session, st *discordgo.ChannelPinsUpdate) {
	go modules.SendEvent(discord.EventTypeChannelPinsUpdate, st)
}
