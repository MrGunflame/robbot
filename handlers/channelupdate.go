package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// ChannelUpdate triggers when a channel is updated
func ChannelUpdate(s *discordgo.Session, st *discordgo.ChannelUpdate) {
	go modules.SendEvent(discord.EventTypeChannelUpdate, st)
}
