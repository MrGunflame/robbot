package handlers

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func VoiceStateChangeHandler(s *discordgo.Session, vs *discordgo.VoiceStateUpdate) {
	fmt.Println(vs.VoiceState)
}
