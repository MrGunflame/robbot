package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// ChannelDelete triggers when a channel is deleted
func ChannelDelete(s *discordgo.Session, st *discordgo.ChannelDelete) {
	go modules.SendEvent(discord.EventTypeChannelDelete, st)
}
