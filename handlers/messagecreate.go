package handlers

import (
	"strings"

	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// MessageCreateHandler is called when a new message is sent
func MessageCreateHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore own messages and other bot messages
	if m.Author.ID != s.State.User.ID && !m.Author.Bot {
		if strings.HasPrefix(m.Content, "!") {
			cmd := m.Content[len("!"):]
			go modules.RunCommand(s, m, cmd)
		}
	}
}
