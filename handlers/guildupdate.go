package handlers

import (
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/modules"

	"github.com/bwmarrin/discordgo"
)

// GuildUpdate triggers when a guild is updated
func GuildUpdate(s *discordgo.Session, st *discordgo.GuildUpdate) {
	go modules.SendEvent(discord.EventTypeGuildUpdate, st)
}
