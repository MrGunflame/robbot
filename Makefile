VERSION := `git describe --tags`
DATE := `date -u +%FT%T%z`

GC := go
GCFLAGS := -gccgoflags "-03 -s -w"
LDFLAGS := -ldflags="-X gitlab.com/MrGunflame/robbot/modules.buildVersion=${VERSION} -X gitlab.com/MrGunflame/robbot/modules.buildDate=${DATE}"

PKGFILES := $$($(GC) list ./... | grep -v /vendor/)

all: configure

build: robbot.go test
	CGO_ENABLED=0 $(GC) build $(GCFLAGS) $(LDFLAGS) -o robbot $<

test: fmt vet
	$(GC) test -race $(PKGFILES)

fmt:
	$(GC) fmt $(PKGFILES)

vet:
	$(GC) vet $(PKGFILES)

dep:
	$(GC) get -v -d ./...

clean:
	$(RM) -f robbot

configure: ./cmd/configure/configure.go ./cmd/configure/gofmt.go
	$(GC) build -o configure $^

.PHONY: all
