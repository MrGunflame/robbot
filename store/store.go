package store

import (
	"database/sql"
	"time"
)

// DBConn is the database connection
var dbConn *sql.DB

// SetDB initializes the sql connection
func SetDB(conn *sql.DB) {
	dbConn = conn
	dbConn.SetConnMaxIdleTime(120 * time.Second)
	dbConn.SetConnMaxLifetime(120 * time.Second)
}

// CloseDB closes the sql connection
func CloseDB() {
	dbConn.Close()
}

// Store contains storage configs for modules
type Store struct {
	Tables []*Table
}
