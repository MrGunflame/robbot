package store

import (
	"database/sql"

	"github.com/go-sql-driver/mysql"
)

// SQLType is a datatype used in a sql column
type SQLType string

// Possible SQL types
const (
	SQLTypeVarchar = "VARCHAR"
)

// SQLFlag is a single column flag
type SQLFlag string

// Possible SQL column flags
const (
	SQLFlagNotNull       SQLFlag = "NOT NULL"
	SQLFlagPrimaryKey    SQLFlag = "PRIMARY KEY"
	SQLFlagUnique        SQLFlag = "UNIQUE"
	SQLFlagAutoIncrement SQLFlag = "AUTO_INCREMENT"
	SQLFlagUnsigned      SQLFlag = "UNSIGNED"
)

// Table is a SQL table used to store data
type Table struct {
	Name    string
	Columns []*Column
}

// Column is a single column in a SQL table
type Column struct {
	Name  string
	Type  string
	Len   int // optional
	Flags []SQLFlag
}

// Insert inserts a new row into a table
// query is the sql query, args the query arguments
// returns an error and the last inserted id
func Insert(query string, args ...interface{}) (int, error) {
	stmt, err := dbConn.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

// Select selects queries the database and returns the rows
// query is the sql query, args the arguments
func Select(query string, args ...interface{}) (*sql.Rows, error) {
	stmt, err := dbConn.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	return stmt.Query(args...)
}

// Delete deletes a set of rows from a table
// query is the sql query, args the query arguments
// returns an error and the number of affected rows
func Delete(query string, args ...interface{}) (int, error) {
	stmt, err := dbConn.Prepare(query)
	if err != nil {
		return 0, err
	}

	res, err := stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	num, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(num), nil
}

// MySQL error codes
const (
	SQLErrDuplicateEntry = 1062
)

// ErrorCode extracts an error code from an mysql error
// panic if the error is not a mysql error (cannot be converted)
func ErrorCode(err error) int {
	dbErr, ok := err.(*mysql.MySQLError)
	if !ok {
		panic("Given error is not of type mysql")
	}
	return int(dbErr.Number)
}
