package store

import (
	"log"
	"strconv"
	"strings"
)

// InitSQLTables will check if all tables required are existent and create them if they are not
func InitSQLTables(tables [][]*Table) error {
	// Get the name of the selected database
	rows, err := Select("SELECT DATABASE()")
	if err != nil {
		return err
	}

	var dbName string
	for rows.Next() {
		if err := rows.Scan(&dbName); err != nil {
			return err
		}
	}

	// The exist query returns 1 row when the table exists
	existStmt, err := dbConn.Prepare("SELECT table_schema FROM information_schema.tables WHERE table_schema = ? AND table_name = ? LIMIT 1")
	if err != nil {
		return err
	}

	for _, m := range tables {
	loop:
		for _, t := range m {
			// Skip if the table has no columns
			if len(t.Columns) < 1 {
				continue
			}

			// Check if the table exists already
			rows, err := existStmt.Query(dbName, t.Name)
			if err != nil {
				return err
			}

			// Skip creating if it exists
			for rows.Next() {
				for rows.Next() {
				}
				continue loop
			}

			writeStrings := func(b *strings.Builder, str ...string) {
				for _, s := range str {
					b.WriteString(s)
				}
			}

			// Build the create query
			var b strings.Builder
			writeStrings(&b, "CREATE TABLE ", t.Name, "(")
			for i, c := range t.Columns {
				writeStrings(&b, c.Name, " ", c.Type)
				if c.Len > 0 {
					writeStrings(&b, "(", strconv.Itoa(c.Len), ")")
				}

				if len(c.Flags) > 0 {
					// Need to append the UNSIGNED attribute first
					for j, f := range c.Flags {
						if f == SQLFlagUnsigned {
							b.WriteString(" ")
							b.WriteString(string(f))
							c.Flags = append(c.Flags[:j], c.Flags[j+1:]...)
							break
						}
					}

					for _, f := range c.Flags {
						b.WriteString(" ")
						b.WriteString(string(f))
						// if j < (len(c.Flags) - 1) {
						// 	b.WriteString(" ")
						// }
					}
				}

				if i < (len(t.Columns) - 1) {
					b.WriteString(",")
				}
			}
			b.WriteString(")")

			log.Println("[INFO/SQL] Creating new table: ", b.String())

			stmt, err := dbConn.Prepare(b.String())
			if err != nil {
				return err
			}

			// Create the table
			if _, err := stmt.Exec(); err != nil {
				return err
			}
		}
	}

	return nil
}
