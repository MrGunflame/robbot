package main

import (
	"io"

	"gitlab.com/MrGunflame/robbot/handlers"
	"gitlab.com/MrGunflame/robbot/modules"
	"gitlab.com/MrGunflame/robbot/modules/permissions"
	"gitlab.com/MrGunflame/robbot/pkg/config"
	"gitlab.com/MrGunflame/robbot/pkg/util"
	"gitlab.com/MrGunflame/robbot/store"

	"database/sql"
	"log"
	"os"
	"os/signal"
	"syscall"

	psql "gitlab.com/MrGunflame/robbot/pkg/sql"

	"github.com/bwmarrin/discordgo"
	_ "github.com/go-sql-driver/mysql"
)

var (
	token string
)

func init() {
	// Load the global mysql config from config/mysql.json
	mysqlconf, err := config.LoadMysqlConfig("config/mysql.json")
	if err != nil {
		log.Fatalf("[FATAL] Failed to load mysql config file: '%s'", err)
	}
	config.DBCon, err = sql.Open("mysql", config.GenerateMysqlLink(mysqlconf))
	if err != nil {
		log.Fatalf("[FATAL] Failed to connect to database: '%s'", err)
	}
	psql.DBConn, err = sql.Open("mysql", config.GenerateMysqlLink(mysqlconf))
	if err != nil {
		log.Fatalf("[FATAL] Failed to connect to database: '%s'", err)
	}
	// if err := psql.SetupDatabase(mysqlconf.Database); err != nil {
	// 	log.Fatalf("[FATAL] Failed to initialize database: '%s'", err)
	// }

	// Setup DB
	conn, err := sql.Open("mysql", config.GenerateMysqlLink(mysqlconf))
	if err != nil {
		log.Fatalf("[FATAL] Failed to initialize database: '%s'", err)
	}
	store.SetDB(conn)
	if err := modules.InitStore(); err != nil {
		log.Fatalf("[FATAL] Failed to setup database: '%s'", err)
	}

	// Load the global bot config from config/bot.json
	botconf, err := config.LoadBotConfig("config/bot.json")
	if err != nil {
		log.Fatalf("[FATAL] Failed to load bot config file: '%s'", err)
	}
	token = botconf.Token
	permissions.BotOwner = botconf.OwnerID

	// Load the server-wide configs from config/servers.json
	serverconf, err := config.LoadServerConfig("config/servers.json")
	if err != nil {
		log.Fatal(err)
	}
	config.Server = serverconf

	// Load variables from environment
	// Used for docker container setup
	// Load all non-empty env vars into ptr
	if len(os.Args) > 1 && os.Args[1] == "env" {
		for _, v := range []struct {
			key      string
			ptr      *string
			required bool
		}{
			{
				key:      "ROBBOT_DISCORD_AUTH_TOKEN",
				ptr:      &token,
				required: true,
			},
			{
				key:      "ROBBOT_DISCORD_OWNER_ID",
				ptr:      &permissions.BotOwner,
				required: false,
			},
		} {
			val := os.Getenv(v.key)
			if val == "" {
				if v.required {
					log.Fatalf("[FATAL] The %s environment variable is not set up", v.key)
				} else {
					log.Printf("[WARN] The %s environment variable is not set up", v.key)
				}
				continue
			}

			if val != "" {
				*v.ptr = val
			}
		}
	}
}

func main() {
	// Open the log file
	file, err := os.OpenFile("latest.log", (os.O_WRONLY + os.O_APPEND), 0)
	if err == os.ErrExist {
		// Create the file
		file, err = os.Create("latest.log")
		if err != nil {
			log.Printf("[WARN] Failed to create log file: '%s'", err)
		}
	} else if err != nil {
		log.Printf("[WARN] Failed to open the log file: '%s'", err)
	} else {
		// Log writes to file and stdout
		log.SetOutput(io.MultiWriter(file, os.Stdout))
	}
	defer file.Close()
	log.SetFlags(log.Ldate + log.Ltime + +log.LUTC + log.Lshortfile)

	// Create a new bot
	dg, err := discordgo.New(util.ConcatStrings("Bot ", token))
	if err != nil {
		log.Fatal(err)
	}

	// Add event handlers
	dg.AddHandler(handlers.MessageCreateHandler)

	dg.AddHandlerOnce(handlers.Ready)

	// open the connection
	err = dg.Open()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("[INFO] Bot is running")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	config.DBCon.Close()
	dg.Close()
	store.CloseDB()
}
